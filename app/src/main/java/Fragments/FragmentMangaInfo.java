package Fragments;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juanma.mangayomu.MangaActivity;
import com.example.juanma.mangayomu.R;

import org.w3c.dom.Text;

import java.io.File;

import Items.MangaItem;
import red.ImageDownloader;
import red.ImageDownloaderMem;

/**
 * Created by juanma on 22/07/14.
 */
public class FragmentMangaInfo extends Fragment {

    //private static final String ARG_SECTION_NUMBER = "section_number";

    //private ImageDownloader imageDownloader;
    private ImageDownloaderMem imageDownloaderMem;
    private MangaItem manga;

    public static FragmentMangaInfo newInstance(MangaItem manga) {
        FragmentMangaInfo fragment = new FragmentMangaInfo(manga);
        fragment.setRetainInstance(true);
        return fragment;
    }

    public FragmentMangaInfo(){}

    public FragmentMangaInfo(MangaItem manga){
        this.manga = manga;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.fragment_manga_info, container, false);

        ImageView image = (ImageView) inflateView.findViewById(R.id.imageMangaInfo);
        TextView titulo = (TextView) inflateView.findViewById(R.id.tittleManga);
        TextView chapters = (TextView) inflateView.findViewById(R.id.chaptersManga);
        TextView clasification = (TextView) inflateView.findViewById(R.id.clasificationManga);
        TextView description = (TextView) inflateView.findViewById(R.id.descriptionManga);

        System.out.println("onCreateView FragmentMangaInfo");

        if(manga != null && manga.getUrl_image() != null) {
            //imageDownloader.download(manga.getUrl_image(), image);
            imageDownloaderMem = new ImageDownloaderMem(getActivity().getFilesDir());
            imageDownloaderMem.download_to_mem(
                    manga.getUrl_image(),
                    manga.getServer_name(),
                    manga.getName(),
                    image
            );
        }
        else {
            image.setImageResource(R.drawable.imagen_no_disponible);
        }
        if(manga != null) {
            titulo.setText(manga.getName());
            chapters.setText(String.valueOf(manga.getList_chapters().size()));
            clasification.setText(String.valueOf(manga.getRanking()));
            description.setText(manga.getDescription());
        }
        else{
            titulo.setText("Unknow");
            chapters.setText("Unknow");
            clasification.setText("Unknow");
            description.setText("Unknow");
        }

        animarVista(image, 1500);
        animarVista(inflateView, 800);

        return inflateView;
    }

    void animarVista(View view, int time) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
        anim.setDuration(time);
        anim.start();
    }
}
