package Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.juanma.mangayomu.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Vector;

import Adapters.FavouriteGridAdapter;
import Items.FavouriteItem;
import Items.MangaItem;
import Items.UserItem;
import dataBase.DataBase;
import red.Utils;
import util.Hash;

/**
 * Created by juanma on 19/08/14.
 */
public class FragmentFavourites extends Fragment{

    public static final String ITEM_NAME_SERVER = "item_name_server";

    Activity actividad;
    GridView gridView;

    private View view;
    private LayoutInflater inflater;
    private ViewGroup container;

    FavouriteGridAdapter adapter;
    OnFavouriteViewListener mCallback;

    private String tittle;

    DataBase db;

    SharedPreferences prefs_user;
    SharedPreferences.Editor editor_user;

    public FragmentFavourites(){}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        actividad = activity;
        try {
            mCallback = (OnFavouriteViewListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + "ha de implementar OnFavouriteViewListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs_user = actividad.getSharedPreferences(UserItem.PREFERENCIAS_DE_USUARIO, Context.MODE_PRIVATE);
        editor_user = prefs_user.edit();

        db = new DataBase(actividad);

        tittle = "Favoritos";
        System.out.println("FragmentFavourite onCreate()");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_favourite, container, false);
        gridView = (GridView) view.findViewById(R.id.favourites_gridLayout);

        getActivity().getActionBar().setTitle(tittle);

        // si acabamos de abrir la aplicación
        if(prefs_user.getBoolean("APLICACION_ABIERTA", false)){
            Log.d("DEBUG_FAVORITOS", "Entramos en actualización de favoritos desde el servidor");
            new GetFavourites().execute();
            editor_user.putBoolean("APLICACION_ABIERTA", false);
            editor_user.commit();
        }
        else {
            Log.d("DEBUG_FAVORITOS", "No estás logueado, así que no descargamos los favoritos de internet");
            adapter = new FavouriteGridAdapter(actividad);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new FavouriteItemClickListener());
            gridView.setOnItemLongClickListener(new FavouriteItemLongClickListener());
        }

        return view;
    }

    private class FavouriteItemClickListener implements GridView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            MangaItem manga;
            if(adapter != null){
                manga = (MangaItem) adapter.getItem(position);
                mCallback.onItemFavouriteSelecter(manga);
            }
        }
    }

    private class FavouriteItemLongClickListener implements GridView.OnItemLongClickListener{
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
            AlertDialog.Builder builder = new AlertDialog.Builder(actividad);
            CharSequence [] items = {"Abrir", "Eliminar de favoritos"};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    MangaItem mangaItem;
                    switch (i){
                        case 0:
                            if(adapter != null){
                                mangaItem = (MangaItem) adapter.getItem(position);
                                mCallback.onItemFavouriteSelecter(mangaItem);
                            }
                            break;
                        case 1:
                            if(adapter != null){
                                mangaItem = (MangaItem) adapter.getItem(position);
                                if(adapter.removeElement(position)){
                                    if(db.removeMangaFavourite(mangaItem.getServer_name(), mangaItem.getName())){
                                        new DeleteFavouriteOnServer(mangaItem.getServer_name(), mangaItem.getName(), adapter).execute();
                                    }
                                }
                            }
                            break;
                    }
                }
            });
            builder.create().show();
            return true;
        }
    }

    public interface OnFavouriteViewListener{
        public void onItemFavouriteSelecter(MangaItem mangaItem);
    }


    public void sincronizar(){
        new GetFavourites().execute();
    }

    class GetFavourites extends AsyncTask<Void, Void, Void> {

        ProgressDialog dialog;
        Vector<MangaItem> listFavouritesDB;
        FavouriteItem listFavouritesWeb;

        boolean sincronizar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(prefs_user.getString("USER_NAME", null) != null){
                listFavouritesDB = db.getFavouriteMangas();

                // Diálogo de espera para esperar a que el servidor responda
                dialog = new ProgressDialog(actividad);
                dialog.setMessage("Actualizando favoritos, esta operación puede llevarle unos minutos");
                dialog.setCancelable(false);
                dialog.show();

                sincronizar = true;
            }
            else{
                Toast.makeText(actividad, "No estás logueado", Toast.LENGTH_SHORT).show();
                sincronizar = false;
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            if(sincronizar) {
                if (!Utils.verificaConexion(actividad)) {
                    cancel(true);
                    return null;
                } else {
                    String user_name = prefs_user.getString("USER_NAME", null);
                    String client_id = prefs_user.getString("CLIENT_ID", null);
                    String pin = prefs_user.getString("PIN", null);
                    String next_random = prefs_user.getString("NEXT_RANDOM", null);
                    String key = Hash.md5(pin + next_random);

                    Log.d("DEBUG_FAVORITOS", "user_name: " + user_name + ", client_id: " + client_id + ", pin: " + pin + ", next_random: " + next_random + ", key: " + key);

                    String responseString = Utils.getFavourites(user_name, client_id, key, true, true);

                    //Log.d("DEBUG_FAVORITOS", "responseString: "+responseString);

                    if (responseString != null) {
                        listFavouritesWeb = new FavouriteItem();
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        try {
                            listFavouritesWeb = gson.fromJson(responseString, FavouriteItem.class);
                        } catch (Exception e) {
                            listFavouritesWeb = null;
                            e.printStackTrace();
                        }

                        if (listFavouritesWeb != null) {
                            if (listFavouritesWeb.getList_mangas() != null) {
                                editor_user.putString("NEXT_RANDOM", listFavouritesWeb.getNext_random());
                                editor_user.commit();
                                shuffle_and_store_in_database_and_server(listFavouritesDB, listFavouritesWeb);
                                Log.d("DEBUG_FAVORITOS", "Hemos salido del barajeo");
                            } else {
                                editor_user.putString("NEXT_RANDOM", listFavouritesWeb.getNext_random());
                                editor_user.commit();
                            }
                        }

                    }
                }
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            dialog.dismiss();

            // Creamos el gridView de los favoritos
            adapter = new FavouriteGridAdapter(actividad);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new FavouriteItemClickListener());
            gridView.setOnItemLongClickListener(new FavouriteItemLongClickListener());
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(sincronizar)
                dialog.dismiss();

            // Creamos el gridView de los favoritos
            adapter = new FavouriteGridAdapter(actividad);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new FavouriteItemClickListener());
            gridView.setOnItemLongClickListener(new FavouriteItemLongClickListener());

        }

        private void shuffle_and_store_in_database_and_server(
                Vector<MangaItem> listFavouritesDB, FavouriteItem listFavouritesWeb){

            boolean primera_conexion;

            if(prefs_user.getBoolean("PRIMERA_CONEXION", false)){
                primera_conexion = true;
                editor_user.putBoolean("PRIMERA_CONEXION", false);
                editor_user.commit();
            }
            else{
                primera_conexion = false;
            }

            // Recolectar mangas que están en local, pero no en el servidor
            for(MangaItem ldb : listFavouritesDB){
                boolean encontrado = false;
                for(int i=0; !encontrado && i<listFavouritesWeb.getList_mangas().size(); i++){
                    if(ldb.getName().equals(listFavouritesWeb.getList_mangas().get(i).getManga_name()) && ldb.getServer_name().equals(listFavouritesWeb.getList_mangas().get(i).getServer_name())){
                        encontrado = true;
                    }
                }
                // si no está en el servidor, lo subimos si es la primera conexión, y si no, lo borramos
                if(!encontrado){
                    // si es primera conexión, lo añadimos al servidor
                    if(primera_conexion) {
                        String user_name = prefs_user.getString("USER_NAME", null);
                        String client_id = prefs_user.getString("CLIENT_ID", null);
                        String pin = prefs_user.getString("PIN", null);
                        String next_random = prefs_user.getString("NEXT_RANDOM", null);
                        String key = Hash.md5(pin + next_random);

                        String responseString = Utils.pushFavourite(
                                user_name,
                                client_id,
                                key,
                                ldb.getServer_name(),
                                ldb.getName(),
                                db.getListNamesChaptersReaded(ldb.getServer_name(), ldb.getName())
                        );

                        if (responseString != null) {
                            UserItem userItem = new UserItem();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();
                            try {
                                userItem = gson.fromJson(responseString, UserItem.class);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (userItem != null) {
                                editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                                editor_user.commit();
                            }

                        }
                    }
                    else{
                        // no es primera conexión, así que hemos de borrarlo de la base de datos local
                        db.removeMangaFavourite(ldb.getServer_name(), ldb.getName());
                    }
                }
            }

            // Descargar mangas que están en el servidor a local
            for(int i=0;i<listFavouritesWeb.getList_mangas().size(); i++){
                String jsonManga;
                String server_name, manga_name;
                server_name = listFavouritesWeb.getList_mangas().get(i).getServer_name();
                manga_name = listFavouritesWeb.getList_mangas().get(i).getManga_name();

                if(!db.existManga(server_name, manga_name)) {
                    jsonManga = Utils.obtenerManga(server_name, manga_name);
                    MangaItem manga = new MangaItem();
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    manga = gson.fromJson(jsonManga, MangaItem.class);
                    db.addManga(manga);
                }
                db.putOnMangaFavourite(server_name, manga_name);

                Log.d("DEBUG_FAVORITOS", "Favorito: " + server_name + ", " + manga_name);
                Vector<String> listChapterReaded;
                Vector<String> listChapterUpdateReaded = new Vector<String>();
                Vector<String> listChapterUpdateNoReaded = new Vector<String>();
                listChapterReaded = db.getListNamesChaptersReaded(server_name, manga_name);
                // Capítulos a añadir
                for(String chapter : listFavouritesWeb.getList_mangas().get(i).getList_chapter_readed()){
                    if(listChapterReaded.indexOf(chapter) == -1){
                        listChapterUpdateReaded.add(chapter);
                        Log.d("DEBUG_FAVORITOS", "Insertar " + chapter);
                    }
                }
                // Capítulos a borrar
                for(String chapter : listChapterReaded){
                    if(listFavouritesWeb.getList_mangas().get(i).getList_chapter_readed().indexOf(chapter) == -1){
                        listChapterUpdateNoReaded.add(chapter);
                        Log.d("DEBUG_FAVORITOS", "Borrar " + chapter);
                    }
                }

                if(listChapterUpdateReaded.size() != 0) {
                    db.putListChaptersReaded(server_name, manga_name, listChapterUpdateReaded, 1);
                }
                if(listChapterUpdateNoReaded.size() != 0) {
                    db.putListChaptersReaded(server_name, manga_name, listChapterUpdateNoReaded, 0);
                }
            }
        }
    }


    class DeleteFavouriteOnServer extends AsyncTask<Void, Void, String>{

        ProgressDialog dialog;

        String server_name;
        String manga_name;
        FavouriteGridAdapter adapter;

        DeleteFavouriteOnServer(String server_name, String manga_name, FavouriteGridAdapter adapter){
            this.server_name = server_name;
            this.manga_name = manga_name;
            this.adapter = adapter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Diálogo de espera para esperar a que el servidor responda
            dialog = new ProgressDialog(actividad);
            dialog.setMessage("Eliminando favorito");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            if (!Utils.verificaConexion(actividad)) {
                cancel(true);
                return null;
            }
            else {
                String user_name = prefs_user.getString("USER_NAME", null);
                // Si estamos logueados
                if(user_name != null) {
                    String client_id = prefs_user.getString("CLIENT_ID", null);
                    String pin = prefs_user.getString("PIN", null);
                    String next_random = prefs_user.getString("NEXT_RANDOM", null);
                    String key = Hash.md5(pin + next_random);

                    String responseString = Utils.removeFavourite(
                        user_name,
                        client_id,
                        key,
                        server_name,
                        manga_name
                    );

                    if (responseString != null) {
                        UserItem userItem = new UserItem();
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        try {
                            userItem = gson.fromJson(responseString, UserItem.class);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (userItem != null) {
                            editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                            editor_user.commit();
                        }
                    }
                }
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            adapter.notifyDataSetChanged();
        }

    }

}
