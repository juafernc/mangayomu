package Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.juanma.mangayomu.CapitulosActivity;
import com.example.juanma.mangayomu.MangaActivity;
import com.example.juanma.mangayomu.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ortiz.touch.TouchImageView;

import java.io.File;

import Adapters.ChapterListAdapter;
import Items.UserItem;
import dataBase.DataBase;
import red.ImageDownloader;
import red.ImageDownloaderCache;
import red.Utils;
import util.Hash;
//import util.ZoomableImageView;

/**
 * Created by juanma on 25/07/14.
 */
public class FragmentChapter extends Fragment {

    private static final String INDEX = "index";

    //private ImageDownloader imageDownloader;
    private int position;
    private File dirCache;

    public TouchImageView image;
    public ProgressBar progressBar;

    DataBase db;


    public static FragmentChapter newInstance(int position) {
        FragmentChapter fragment = new FragmentChapter();

        Bundle args = new Bundle();
        args.putInt(INDEX, position);
        fragment.setArguments(args);
        //fragment.setRetainInstance(true);

        return fragment;
    }

    public FragmentChapter(){
        //imageDownloader = new ImageDownloader();
        //position = 0;
    }

    /*public FragmentChapter(int position) {
        imageDownloader = new ImageDownloader();
        //this.position = position;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        position = (getArguments() != null) ? getArguments().getInt(INDEX) : -1;
    }

    /*public void setPosition(int position) {
        this.position = position;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.fragment_chapter, container, false);

        //position = CapitulosActivity.mViewPager.getCurrentItem();
        System.out.println("Position in FragmentChapter = " + position);

        dirCache = getActivity().getCacheDir();
        String nameFich = CapitulosActivity.nameServer+CapitulosActivity.nameManga+CapitulosActivity.nameChapter;
        File fich = new File(dirCache, nameFich+"-"+String.valueOf(position)+".jpg");

        image = (TouchImageView) inflateView.findViewById(R.id.imageChapter);
        image.setScaleType(ImageView.ScaleType.MATRIX);

        progressBar = (ProgressBar) inflateView.findViewById(R.id.progresLoadingChapter);
        //Button next = (Button) inflateView.findViewById(R.id.button_right);
        //Button previous = (Button) inflateView.findViewById(R.id.button_left);

        if (CapitulosActivity.chapter.getList_images() != null) {
            //imageDownloader.download(CapitulosActivity.chapter.getList_images().elementAt(position), image);
            if(fich.exists()){
                System.out.println("La imagen " + fich.toString() + ", existe");
                try {
                    (new AsyncTask<String, Void, Void>(){

                        Bitmap bitmap;

                        @Override
                        protected Void doInBackground(String... strings) {
                            String fich = strings[0];
                            try {
                                bitmap = BitmapFactory.decodeFile(String.valueOf(fich));
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            Log.d("DEBUG_CHAPTER", "Llego aquí");
                            progressBar.setVisibility(View.GONE);
                            image.setVisibility(View.VISIBLE);
                            image.setImageBitmap(bitmap);
                        }
                    }).execute(fich.toString());
                    //Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(fich));
                    //image.setImageBitmap(bitmap);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            else{
                System.out.println("La imagen " + fich.toString() + ", NO existe");
                //imageDownloader.download(CapitulosActivity.chapter.getList_images().elementAt(position), image);
                System.out.println("La imagen " + position + ": " + ImageDownloaderCache.enlaces_rotos);
                if(ImageDownloaderCache.enlaces_rotos != null && ImageDownloaderCache.enlaces_rotos.get(position)){
                    progressBar.setVisibility(View.GONE);
                    image.setVisibility(View.VISIBLE);
                    image.setImageResource(R.drawable.imagen_no_encontrada);
                }
            }
        } else {
            progressBar.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
            image.setImageResource(R.drawable.imagen_no_disponible);
        }

        image.setOnTouchListener(new View.OnTouchListener() {
            GestureDetector gd = new GestureDetector(new MyGestureDetector());
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gd.onTouchEvent(motionEvent);
            }
        });

        /*image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // si está en fullscreen
                    if (CapitulosActivity.uiOption == getActivity().getWindow().getDecorView().getSystemUiVisibility()) {
                        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                        (new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                getActivity().getWindow().getDecorView().setSystemUiVisibility(CapitulosActivity.uiOption);
                            }
                        }).execute();
                    } else {
                        getActivity().getWindow().getDecorView().setSystemUiVisibility(CapitulosActivity.uiOption);
                    }
                }catch(Exception e){
                    //e.printStackTrace();
                }
            }
        });*/

        return inflateView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.d("DEBUG_PAGER", CapitulosActivity.mViewPager.getCurrentItem() +"/"+ (CapitulosActivity.chapter.getList_images().size()-1));
    }

    public class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            int x;
            int margenDer, margenIzq;
            x = (int) event.getX();

            DisplayMetrics display = getActivity().getResources().getDisplayMetrics();
            margenDer = (int) (display.widthPixels / 1.3);
            margenIzq = display.widthPixels - margenDer;
            if(x <= margenIzq){
                CapitulosActivity.mViewPager.setCurrentItem(position-1);
            }
            else if(x >= margenDer){
                CapitulosActivity.mViewPager.setCurrentItem(position+1);
            }

            return false;
        }
    }



}
