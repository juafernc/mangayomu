package Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.juanma.mangayomu.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Vector;

import Adapters.CustomListCenterAdapter;
import Items.MangaItem;
import Items.ServerItem;
import dataBase.DataBase;
import red.Utils;

/**
 * Created by juanma on 20/07/14.
 */
public class FragmentCenter extends Fragment{

    public static final String ITEM_NAME_SERVER = "item_name_server";
    public static final String ITEM_NAME_MANGA = "item_name_manga";

    Activity actividad;
    ListView listView;

    private View view;
    private LayoutInflater inflater;
    private ViewGroup container;

    public static CustomListCenterAdapter adapter;

    OnListCenterViewListener mCallback;

    //ListMangas mangas;
    ServerItem mangas;
    GetListMangas getListMangas;

    ProgressDialog dialog;
    DataBase db;

    //public static final String ITEM_NAME = "itemName";
    //public static final String ITEM_POS = "itemPos";

    private String nameServer;
    //private int posicion;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    SearchView search;

    public FragmentCenter(SearchView searchView) {
        this.search = searchView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        actividad = activity;
        try {
            mCallback = (OnListCenterViewListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + "ha de implementar OnListCenterViewListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getActivity().getSharedPreferences("PreferenciasMangaYomu", Context.MODE_PRIVATE);
        editor = prefs.edit();

        nameServer = getArguments().getString(ITEM_NAME_SERVER);
        System.out.println("FragmentCenter onCreate()");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_list_center, container, false);
        listView = (ListView) view.findViewById(R.id.listCenter);
        //search = (SearchView) view.findViewById(R.id.menu_buscar);
        //search = (EditText) view.findViewById(R.id.menu_buscar);
        System.out.println("listView creado en FragmentCenter");

        getActivity().getActionBar().setTitle(nameServer);

        getListMangas = new GetListMangas();
        getListMangas.execute();

        //Toast.makeText(actividad, searchView.getQuery(), Toast.LENGTH_SHORT).show();

        listView.setOnItemClickListener(new ListCenterItemClickListener());
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(dialog != null){
            dialog.dismiss();
        }
        if(getListMangas != null) {
            getListMangas.cancel(true);
        }
    }

    private class ListCenterItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //mCallback.onItemCenterSelecter(position, mangas.getList_mangas().get(position));
            mCallback.onItemCenterSelecter(mangas.getList_mangas().indexOf(adapter.getItem(position)), adapter.getItem(position));
        }
    }

    public interface OnListCenterViewListener{
        public void onItemCenterSelecter(int position, String nameManga);
    }

    class GetListMangas extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialog = ProgressDialog.show(actividad, "", "Cargando mangas", true);
            dialog = new ProgressDialog(actividad);
            dialog.setMessage("Cargando mangas");
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            db = new DataBase(actividad);
            mangas = db.getListMangasFromServer(nameServer);
            if(mangas != null){
                System.out.println("La lista de mangas existen en la base de datos");
                return null;
            }
            else{
                if(!Utils.verificaConexion(actividad)){
                    cancel(true);
                    return null;
                }
                else{
                    String responseString = Utils.obtenerListaMangas(nameServer);
                    return responseString;
                }
            }
        }
        @Override
        protected void onPostExecute(String jsonString) {
            if(jsonString != null){
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                mangas = gson.fromJson(jsonString, ServerItem.class);
                System.out.println("Lista de mangas descargada de internet");
                Toast.makeText(actividad, "Lista de mangas descargada de internet", Toast.LENGTH_LONG).show();

                if(mangas != null){
                    System.out.println("Añadiendo lista de mangas a la base de datos");
                    db.addServer(mangas);
                }
            }
            if(mangas != null){
                adapter = new CustomListCenterAdapter(actividad, mangas.getList_mangas());
                if(listView!=null)
                    listView.setAdapter(adapter);
                else
                    System.out.println("listView is null!!");
            }
            else{
                Toast.makeText(actividad, "ERROR inesperado", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
            dialog.dismiss();
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
        }
    }
}
