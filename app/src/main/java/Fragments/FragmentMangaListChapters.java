package Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.juanma.mangayomu.MangaActivity;
import com.example.juanma.mangayomu.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;
import java.util.Random;

import Adapters.ChapterListAdapter;
import Adapters.CustomListCenterAdapter;
import Adapters.MangaPagerAdapter;
import Items.MangaItem;
import dataBase.DataBase;
import red.Utils;

/**
 * Created by juanma on 22/07/14.
 */
public class FragmentMangaListChapters extends Fragment{

    private static final String ITEM_NAME_SERVER = "item_name_server";
    private static final String ITEM_NAME_MANGA = "item_name_manga";

    private String nameServer;
    private String nameManga;

    private MangaItem manga;

    Activity actividad;
    ListView listView;

    public SwipeRefreshLayout swipeRefreshLayout;

    public static ChapterListAdapter adapter;

    public OnListChaptersViewListener mCallback;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    //private Integer variable_random_debuger;

    public static FragmentMangaListChapters newInstance(MangaItem manga) {
        Bundle args = new Bundle();
        args.putString(ITEM_NAME_SERVER, manga.getServer_name());
        args.putString(ITEM_NAME_MANGA, manga.getName());
        FragmentMangaListChapters fragment = new FragmentMangaListChapters(manga);
        //System.out.println("newInstance FragmentMangaListChapters");
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    public FragmentMangaListChapters(){
        //variable_random_debuger = 666;
    }

    public FragmentMangaListChapters(MangaItem manga){
        this.manga = manga;
        //variable_random_debuger = Integer.valueOf(new Random().nextInt());
    }

    public ChapterListAdapter getAdapter() {
        //System.out.println("Valor del adapter = " + adapter);
        return adapter;
    }

    /*public Integer getVariable_random_debuger() {
        return variable_random_debuger;
    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        actividad = activity;
        try {
            mCallback = (OnListChaptersViewListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + "ha de implementar OnListChaptersViewListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getActivity().getSharedPreferences("PreferenciasMangaYomu", Context.MODE_PRIVATE);
        editor = prefs.edit();

        nameServer = getArguments().getString(ITEM_NAME_SERVER);
        nameManga = getArguments().getString(ITEM_NAME_MANGA);

        if(nameServer == null || nameServer.equals("Mangayomu")) nameServer = prefs.getString(ITEM_NAME_SERVER,null);
        if(nameManga == null) nameManga = prefs.getString(ITEM_NAME_MANGA,null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.fragment_manga_list_chapters, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) inflateView.findViewById(R.id.swipe_container);

        listView = (ListView) inflateView.findViewById(R.id.listChapters);
        adapter = new ChapterListAdapter(actividad, manga, nameServer);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new ListChaptersItemClickListener());

        swipeRefreshLayout.setColorScheme(
                android.R.color.holo_purple,
                android.R.color.black,
                android.R.color.holo_purple,
                android.R.color.white
        );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                UpdateManga updateManga = new UpdateManga(actividad);
                updateManga.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        return inflateView;
        //return swipeRefreshLayout;
    }

/*    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listView = new ListView(getActivity());
        adapter = new ChapterListAdapter(actividad, manga, nameServer);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new ListChaptersItemClickListener());

        swipeRefreshLayout.addView(listView);
    }
*/
    @Override
    public void onResume() {
        super.onResume();
        Log.d("DEBUG_onResume", "FragmentMangaListChapters -> onResume()");

        if(adapter == null || listView == null) {
            adapter = new ChapterListAdapter(actividad, manga, nameServer);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new ListChaptersItemClickListener());
        }

        adapter.UpdateList();
    }

    private class ListChaptersItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mCallback.onItemChapterSelecter(position);
        }
    }

    public interface OnListChaptersViewListener{
        public void onItemChapterSelecter(int position);
    }


    class UpdateManga extends AsyncTask<Void, Void, String>{
        Activity activity;
        DataBase db;

        UpdateManga(Activity activity){
            this.activity = activity;
            db = new DataBase(activity);
        }

        @Override
        protected String doInBackground(Void... voids) {

            if (!Utils.verificaConexion(actividad)) {
                cancel(true);
                return null;
            } else {
                String responseString = Utils.obtenerManga(nameServer, nameManga);
                return responseString;
            }

        }

        @Override
        protected void onPostExecute(String jsonString) {

            MangaItem newManga = new MangaItem();

            if (jsonString != null) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                newManga = gson.fromJson(jsonString, MangaItem.class);

                System.out.println("Descargado de internet");
                Toast.makeText(actividad, "Descargado de internet", Toast.LENGTH_LONG).show();

                if(newManga != null && manga != null){
                    shuffle_and_store_in_database(manga, newManga);
                }
            }

            swipeRefreshLayout.setRefreshing(false);

        }

        @Override
        protected void onCancelled(String s) {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            swipeRefreshLayout.setRefreshing(false);
        }

        private void shuffle_and_store_in_database(MangaItem _old, MangaItem _new){

            if(_new != null && _new.getList_chapters() != null && _new.getList_chapters().size() > 0) { // la base de datos no está vacía, por lo que hay que barajar y añadir y/o quitar los capítulos correspondientes

                if (_old.getList_chapters().indexOf(_new.getList_chapters().get(_new.getList_chapters().size()-1)) == -1) {
                    db.addManga(_new);
                    Collections.reverse(_new.getList_chapters());
                    manga = _new;
                    getAdapter().setMangaItem(manga); // actializamos la lista del adapter
                }

            }
        }
    }
}
