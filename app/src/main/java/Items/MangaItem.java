package Items;

import java.util.List;
import java.util.Vector;

/**
 * Created by juanma on 23/07/14.
 */
public class MangaItem {
    private String server_name;
    private String name;
    private String url_image;
    private String url_server_manga;
    private String description;
    private Vector<String> list_chapters;
    private String ranking;

    // campo adicional que no está en el json, pero necesario para favoritos
    //private String nameServer;


    public String getServer_name() {
        return server_name;
    }

    public void setServer_name(String server_name) {
        this.server_name = server_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getUrl_server_manga() {
        return url_server_manga;
    }

    public void setUrl_server_manga(String url_server_manga) {
        this.url_server_manga = url_server_manga;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Vector<String> getList_chapters() {
        return list_chapters;
    }

    public void setList_chapters(Vector<String> list_chapters) {
        this.list_chapters = list_chapters;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    /*public String getNameServer() {
        return nameServer;
    }

    public void setNameServer(String nameServer) {
        this.nameServer = nameServer;
    }*/
}
