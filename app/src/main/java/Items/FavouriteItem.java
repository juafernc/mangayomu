package Items;

import java.util.Vector;

/**
 * Created by juanma on 26/11/14.
 */
public class FavouriteItem {

    public class mangas {
        String server_name;
        String manga_name;

        Vector<String> list_chapter_readed;
        int numchapters;

        String imgurl;

        public String getServer_name() {
            return server_name;
        }

        public String getManga_name() {
            return manga_name;
        }

        public Vector<String> getList_chapter_readed() {
            return list_chapter_readed;
        }

        public int getNumchapters() {
            return numchapters;
        }

        public String getImgurl() {
            return imgurl;
        }
    }

    Vector<mangas> list_mangas;
    String next_random;

    public Vector<mangas> getList_mangas() {
        return list_mangas;
    }

    public String getNext_random() {
        return next_random;
    }
}
