package Items;

import java.util.Vector;

/**
 * Created by juanma on 10/08/14.
 */
public class ServersItem {
    Vector<String> list_servers;

    public Vector<String> getList_servers() {
        return list_servers;
    }

    public void setList_servers(Vector<String> list_servers) {
        this.list_servers = list_servers;
    }
}
