package Items;

import java.util.Vector;

/**
 * Created by juanma on 19/07/14.
 */
public class ServerItem {
    String name;
    String url;
    Vector<String> list_mangas;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Vector<String> getList_mangas() {
        return list_mangas;
    }

    public void setList_mangas(Vector<String> list_mangas) {
        this.list_mangas = list_mangas;
    }
}