package Items;

import java.util.Vector;

/**
 * Created by juanma on 24/07/14.
 */
public class ChapterItem {
    Vector<String> list_images;

    public Vector<String> getList_images() {
        return list_images;
    }

    public void setList_images(Vector<String> list_images) {
        this.list_images = list_images;
    }
}
