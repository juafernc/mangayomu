package Items;

/**
 * Created by juanma on 20/08/14.
 */
public class DrawerItem {
    String server;
    String tittle;
    boolean isTittle;

    public DrawerItem(String nameItem, boolean isTittle){
        this.isTittle = isTittle;
        if(isTittle){
            this.tittle = nameItem;
        }
        else{
            this.server = nameItem;
        }
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public boolean isTittle() {
        return isTittle;
    }

    public void setTittle(boolean isTittle) {
        this.isTittle = isTittle;
    }
}
