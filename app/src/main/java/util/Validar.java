package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by juanma on 6/01/15.
 */
public class Validar {

    private static final String PATTERN_MAIL = "^[-\\w\\+]+(\\.[-\\w\\+]+)*@[-\\w]+(\\.[-\\w]+)*(\\.[A-Za-z]{2,})$";
    private static final String PATTERN_USER = "\\w{3,20}";

    static public boolean Mail(String mail){
        Pattern pattern = Pattern.compile(PATTERN_MAIL);

        Matcher matcher = pattern.matcher(mail);

        return matcher.matches();
    }

    static public boolean User(String user){
        Pattern pattern = Pattern.compile(PATTERN_USER);

        Matcher matcher = pattern.matcher(user);

        return matcher.matches();
    }
}
