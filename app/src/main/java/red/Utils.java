package red;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by juanma on 21/07/14.
 */
public class Utils {
    public static boolean verificaConexion(Context ctx) {
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        for (int i = 0; i < 2; i++) {
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    /*
    * Respuestas de un HttpResponse
    *
    * */

    public static String responseToString(HttpResponse response){
        String responseString = null;

        try {
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseString;
    }

    /*
    * PETICIONES REST PARA EL SERVIDOR DE MANGAS
    *
    * */

    public static String obtenerListaServidores(){
        String responseString = null;
        HttpResponse response = null;

        try {
            HttpGet get = new HttpGet("http://agaman.me:4567/server");
            response = (new DefaultHttpClient()).execute(get);
/*            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    public static String obtenerListaMangas(String server){
        String responseString = null;
        HttpResponse response = null;

        try {
            server = URLEncoder.encode(server, "UTF-8").replace("+","%20");
            HttpGet get = new HttpGet("http://agaman.me:4567/server/"+server);
            response = (new DefaultHttpClient()).execute(get);
            /*HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }


    public static String obtenerManga(String server, String manga){
        String responseString = null;
        HttpResponse response = null;

        try {
            server = URLEncoder.encode(server, "UTF-8").replace("+","%20");
            manga = URLEncoder.encode(manga, "UTF-8").replace("+","%20");
            String url = "http://agaman.me:4567/manga/"+server+"/"+manga;
            Log.d("peticiones","obtenerManga: "+ url);
            HttpGet get = new HttpGet(url);
            response = (new DefaultHttpClient()).execute(get);
            /*HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    public static String obtenerCapitulosManga(String server, String manga, String chapter){
        String responseString = null;
        HttpResponse response = null;

        try {
            server = URLEncoder.encode(server, "UTF-8").replace("+","%20");
            manga = URLEncoder.encode(manga, "UTF-8").replace("+","%20");
            chapter = URLEncoder.encode(chapter, "UTF-8").replace("+","%20");
            String url = "http://agaman.me:4567/manga/"+server+"/"+manga+"/"+chapter;
            HttpGet get = new HttpGet(url);
            response = (new DefaultHttpClient()).execute(get);
            /*HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    /*
    * PETICIONES REST PARA EL USUARIO
    *
    * */

    public static HttpResponse logearse(String user, String mail){
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://agaman.me:4567/user");

        HttpResponse response = null;

        try{
            List<NameValuePair> pairs = new ArrayList<NameValuePair>(2);
            if(user != null)
                pairs.add(new BasicNameValuePair("user_name",user));
            pairs.add(new BasicNameValuePair("mail",mail));
            //pairs.add(new BasicNameValuePair("user_name","juanmafn"));
            //pairs.add(new BasicNameValuePair("mail","Megalinksmusic.000.uxidx3v1hmt@gmail.com"));
            post.setEntity(new UrlEncodedFormEntity(pairs));

            response = client.execute(post);

        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        return response;

        /*Log.d("DEBUG_RED", "COD: "+response.getStatusLine().getStatusCode());
        HttpEntity entity = response.getEntity();

        if(entity != null){
            InputStream stream = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            stream.close();
            responseString = sb.toString();
        }*/
    }

    public static String testLogin(String client_id, String key){
        String responseString = null;
        HttpResponse response = null;

        try {
            String url = "http://agaman.me:4567/test_login?client_id="+client_id+"&key="+key;
            HttpGet get = new HttpGet(url);
            response = (new DefaultHttpClient()).execute(get);
            /*HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    public static String logout(String client_id, String key){
        String responseString = null;
        HttpResponse response = null;

        try {
            String url = "http://agaman.me:4567/logout?client_id="+client_id+"&key="+key;
            HttpGet get = new HttpGet(url);
            response = (new DefaultHttpClient()).execute(get);
            /*HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }


    public static String getFavourites(String user, String client_id, String key, boolean addreaded, boolean addextrainfo){
        String responseString = null;
        HttpResponse response = null;

        try {
            String url = "http://www.agaman.me:4567/favourite/"+user+"?client_id="+client_id+"&key="+key+"&addreaded="+addreaded+"&addextrainfo="+addextrainfo;
            HttpGet get = new HttpGet(url);
            response = (new DefaultHttpClient()).execute(get);
            /*HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    public static String pushFavourite(String user, String client_id, String key, String server, String manga, Vector<String> chapters){
        String responseString = null;
        HttpResponse response = null;

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://agaman.me:4567/favourite/"+user);

        try{
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("user",user));
            pairs.add(new BasicNameValuePair("client_id",client_id));
            pairs.add(new BasicNameValuePair("key",key));

            pairs.add(new BasicNameValuePair("server",server));
            pairs.add(new BasicNameValuePair("manga",manga));
            if(chapters != null){
                for(String chapter : chapters) {
                    pairs.add(new BasicNameValuePair("chapter[]", chapter));
                }
            }

            post.setEntity(new UrlEncodedFormEntity(pairs));

            response = client.execute(post);
            /*HttpEntity entity = response.getEntity();

            if(entity != null){
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/

        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    public static String removeFavourite(String user, String client_id, String key, String server, String manga){
        String responseString = null;
        HttpResponse response = null;

        try {
            server = URLEncoder.encode(server, "UTF-8").replace("+","%20");
            manga = URLEncoder.encode(manga, "UTF-8").replace("+","%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("client_id",client_id));
        pairs.add(new BasicNameValuePair("key",key));

        HttpClient client = new DefaultHttpClient();
        HttpDelete delete = new HttpDelete("http://agaman.me:4567/favourite/"+user+"/"+server+"/"+manga + "?" + URLEncodedUtils.format(pairs, "utf-8"));

        try{
            response = client.execute(delete);
            /*HttpEntity entity = response.getEntity();

            if(entity != null){
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/

        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    public static String modifyChapterReaded(String user, String client_id, String key, String server, String manga, Vector<String> chapters, String type){
        String responseString = null;
        HttpResponse response = null;

        try {
            server = URLEncoder.encode(server, "UTF-8").replace("+","%20");
            manga = URLEncoder.encode(manga, "UTF-8").replace("+","%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://agaman.me:4567/favourite/"+user+"/"+server+"/"+manga);

        try{
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("user",user));
            pairs.add(new BasicNameValuePair("client_id",client_id));
            pairs.add(new BasicNameValuePair("key",key));

            pairs.add(new BasicNameValuePair("server",server));
            pairs.add(new BasicNameValuePair("manga",manga));
            if(chapters != null){
                for(String chapter : chapters) {
                    pairs.add(new BasicNameValuePair("chapter[]", chapter));
                }
            }

            pairs.add(new BasicNameValuePair("type",type));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            response = client.execute(post);
            /*HttpEntity entity = response.getEntity();

            if(entity != null){
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/

        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

    public static String modifyChapterReaded(String user, String client_id, String key, String server, String manga, String chapter, String type){
        String responseString = null;
        HttpResponse response = null;

        try {
            server = URLEncoder.encode(server, "UTF-8").replace("+","%20");
            manga = URLEncoder.encode(manga, "UTF-8").replace("+","%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://agaman.me:4567/favourite/"+user+"/"+server+"/"+manga);

        try{
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("user",user));
            pairs.add(new BasicNameValuePair("client_id",client_id));
            pairs.add(new BasicNameValuePair("key",key));
            pairs.add(new BasicNameValuePair("chapter", chapter));
            pairs.add(new BasicNameValuePair("type",type));

            post.setEntity(new UrlEncodedFormEntity(pairs));

            response = client.execute(post);
            /*HttpEntity entity = response.getEntity();

            if(entity != null){
                InputStream stream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                stream.close();
                responseString = sb.toString();
            }*/

        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        responseString = responseToString(response);

        return responseString;
    }

}
