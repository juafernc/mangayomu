package red;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.juanma.mangayomu.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import Adapters.ChapterPagerAdapter;

/**
 * Created by juanma on 30/07/14.
 */
public class ImageDownloaderCache {

    private static final String LOG_TAG = "ImageDownloaderCache";
    private BitmapDownloaderTask task;
    private File dirCache;

    static public Hashtable<Integer, Boolean> enlaces_rotos;


    public ImageDownloaderCache(File dir){
        this.dirCache = dir;
    }

    public void cancellTask(){
        if(task != null) {
            task.cancel(true);
        }
    }

    public void download_to_cache(ViewPager pagerChapter, ChapterPagerAdapter chapterPagerAdapter, List<String> urls, String nameServer, String nameManga, String nameChapter) {
        task = new BitmapDownloaderTask(pagerChapter, chapterPagerAdapter, urls);
        //task.execute(nameServer + nameManga + nameChapter);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, nameServer + nameManga + nameChapter);
    }

    Bitmap downloadBitmap(String url) {
        // AndroidHttpClient is not allowed to be used from the main thread

        AndroidHttpClient client = null;
        HttpGet getRequest = null;

        try {
            client = AndroidHttpClient.newInstance("Android");
            url = URLEncoder.encode(url, "UTF-8").replace("+","%20").replace("%3A", ":").replace("%2F","/");
            getRequest = new HttpGet(url);

            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    inputStream = entity.getContent();
                    // return BitmapFactory.decodeStream(inputStream);
                    // Bug on slow connections, fixed in future release.
                    return BitmapFactory.decodeStream(new FlushedInputStream(inputStream));
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (IOException e) {
            getRequest.abort();
            Log.w(LOG_TAG, "I/O error while retrieving bitmap from " + url, e);
        } catch (IllegalStateException e) {
            getRequest.abort();
            Log.w(LOG_TAG, "Incorrect URL: " + url);
        } catch (Exception e) {
            getRequest.abort();
            Log.w(LOG_TAG, "Error while retrieving bitmap from " + url, e);
        } finally {
            if ((client instanceof AndroidHttpClient)) {
                ((AndroidHttpClient) client).close();
            }
        }
        return null;
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    class BitmapDownloaderTask extends AsyncTask<String, Object, Void> {
        private List<String> urls;
        private String nameFich;
        Bitmap bitmap;
        ViewPager pagerChapter;
        ChapterPagerAdapter chapterPagerAdapter;

        BitmapDownloaderTask(ViewPager pager, ChapterPagerAdapter adapter, List<String> urls){
            pagerChapter = pager;
            chapterPagerAdapter = adapter;
            this.urls = urls;
            enlaces_rotos = new Hashtable<Integer, Boolean>();
            for(int i=0;i<urls.size();i++){
                enlaces_rotos.put(i,false);
            }
        }

        @Override
        protected Void doInBackground(String... strings) {
            nameFich = strings[0];
            int ima=0;
            for(String url : urls) {
                if(!isCancelled()) {
                    System.out.println("Empezando a descargar imagen " + url);


                    File fich = new File(dirCache, nameFich + "-" + String.valueOf(ima) + ".jpg");
                    if (!fich.exists()) {
                        bitmap = downloadBitmap(url);
                        FileOutputStream fout = null;
                        try {
                            fout = new FileOutputStream(fich);
                            Log.d("DEBUG_IMAGEN", "position: "+ima+", bitmap: " + bitmap);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fout);
                            fout.flush();
                        } catch (Exception e) {
                            e.printStackTrace();
                            fich.delete();  // borramos fichero ya que ha fallado y puede que esté incompleto
                            //cancel(true); // si algo sale mal cancelamos
                            enlaces_rotos.put(ima, true);
                        } finally {
                            if (fout != null) {
                                try {
                                    fout.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    } else {
                        //Existe la imagen, así que hay que leerla de cache
                        bitmap = BitmapFactory.decodeFile(String.valueOf(fich));
                    }
                    // si la imagen que acabo de obtener, está visible en el visor del pager, le asigno la imagen a su imageview
                    if (ima == pagerChapter.getCurrentItem() || ima == pagerChapter.getCurrentItem()+1 || ima == pagerChapter.getCurrentItem()-1) {
                        publishProgress(ima, bitmap);
                    }
                    ima++;

                    System.out.println("Descargada imagen " + url);
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            Integer position = (Integer) values[0];
            Bitmap image = (Bitmap) values[1];
            System.out.println("Asignamos imagen en la página " + position);
            if(chapterPagerAdapter != null && chapterPagerAdapter.getFragments() != null &&
                    chapterPagerAdapter.getFragments().get(position) != null &&
                    chapterPagerAdapter.getFragments().get(position).image != null) {
                chapterPagerAdapter.getFragments().get(position).progressBar.setVisibility(View.GONE);
                chapterPagerAdapter.getFragments().get(position).image.setVisibility(View.VISIBLE);
                if(image == null){
                    chapterPagerAdapter.getFragments().get(position).image.setImageResource(R.drawable.imagen_no_encontrada);
                }
                else {
                    chapterPagerAdapter.getFragments().get(position).image.setImageBitmap(image);
                }
            }
        }

    }

}
