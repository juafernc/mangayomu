package red;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.ImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import Adapters.ChapterPagerAdapter;

/**
 * Created by juanma on 20/08/14.
 */
public class ImageDownloaderMem {

    private static final String LOG_TAG = "ImageDownloaderMem";
    private BitmapDownloaderTask task;
    private File dirMem;

    public ImageDownloaderMem(File dir){
        this.dirMem = dir;
    }

    public void cancellTask(){
        if(task != null) {
            task.cancel(true);
        }
    }

    public void download_to_mem(String url, String nameServer, String nameManga, ImageView imageView) {
        task = new BitmapDownloaderTask(url, imageView);
        //task.execute(nameServer+nameManga);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, nameServer+nameManga);
    }

    Bitmap downloadBitmap(String url) {
        // AndroidHttpClient is not allowed to be used from the main thread
        final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
        final HttpGet getRequest = new HttpGet(url);

        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    inputStream = entity.getContent();
                    // return BitmapFactory.decodeStream(inputStream);
                    // Bug on slow connections, fixed in future release.
                    return BitmapFactory.decodeStream(new FlushedInputStream(inputStream));
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (IOException e) {
            getRequest.abort();
            Log.w(LOG_TAG, "I/O error while retrieving bitmap from " + url, e);
        } catch (IllegalStateException e) {
            getRequest.abort();
            Log.w(LOG_TAG, "Incorrect URL: " + url);
        } catch (Exception e) {
            getRequest.abort();
            Log.w(LOG_TAG, "Error while retrieving bitmap from " + url, e);
        } finally {
            if ((client instanceof AndroidHttpClient)) {
                ((AndroidHttpClient) client).close();
            }
        }
        return null;
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    class BitmapDownloaderTask extends AsyncTask<String, Void, Void> {
        private String url;
        private ImageView imageView;
        private String nameFich;
        Bitmap bitmap;

        BitmapDownloaderTask(String url, ImageView imageView){
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Void doInBackground(String... strings) {
            System.out.println("###################### entramos a conseguir la imagen del manga de favoritos");
            nameFich = strings[0];
            if(!isCancelled()) {
                File fich = new File(dirMem, nameFich + ".jpg");
                if (!fich.exists()) {
                    bitmap = downloadBitmap(url);
                    FileOutputStream fout = null;
                    try {
                        fout = new FileOutputStream(fich);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fout);
                        fout.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                        fich.delete();
                        cancel(true); // si algo sale mal cancelamos
                    } finally {
                        if (fout != null) {
                            try {
                                fout.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } else {
                    //Existe la imagen, así que hay que leerla de cache
                    bitmap = BitmapFactory.decodeFile(String.valueOf(fich));
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(isCancelled()){
                bitmap = null;
            }

            if(imageView != null && bitmap != null){
                imageView.setImageBitmap(bitmap);
            }
        }
    }

}

