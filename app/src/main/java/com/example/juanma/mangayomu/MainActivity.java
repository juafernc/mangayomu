package com.example.juanma.mangayomu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;

import java.util.Vector;

import Adapters.CustomDrawerAdapter;
import Fragments.FragmentCenter;
import Fragments.FragmentFavourites;
import Items.DrawerItem;
import Items.MangaItem;
import Items.ServersItem;
import Items.UserItem;
import dataBase.DataBase;
import red.Utils;
import util.Hash;
import util.Validar;


public class MainActivity extends Activity implements FragmentCenter.OnListCenterViewListener, FragmentFavourites.OnFavouriteViewListener{

    private static final String ITEM_NAME_SERVER = "item_name_server";
    private static final String ITEM_NAME_MANGA = "item_name_manga";
    private static final String CLASIFICATION_MANGA = "clasification_manga";

    private final int INDICE_SESION = 0;
    private final int INDICE_PERSONAL = 1;
    private final int INDICE_SERVIDORES = 5;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    CustomDrawerAdapter adapter;

    ServersItem servers;
    GetListServers getListServers;

    SharedPreferences prefs, prefs_user;
    SharedPreferences.Editor editor, editor_user;

    DataBase db;

    SearchManager searchManager;
    SearchView searchView;

    public String nameServer = null;

    boolean firstDownload;
    boolean inFavorites;

    private Activity actividad = this;

    FragmentFavourites fragmentFavourite;

    AlertDialog.Builder alert_inicio_sesion;
    AlertDialog.Builder alert_introducir_pin;
    AlertDialog.Builder alert_cerrar_sesion;
    AlertDialog.Builder alert_login_correcto;
    AlertDialog.Builder alert_desconexion_fallida;
    AlertDialog.Builder alert_desconexion;

    ProgressDialog dialog_inicio_sesion;
    ProgressDialog dialog_cerrar_sesion;
    ProgressDialog dialog_testLogin;

    private final int maximo_intentos_introducir_pin = 3;
    private int intentos_introducir_pin = 0;

    private final int TIME_TO_CLOSE_APP = 2000;
    private long currentTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("ROTATION","MainActivity: onCreate");
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences("PreferenciasMangaYomu", Context.MODE_PRIVATE);
        editor = prefs.edit();

        prefs_user = getSharedPreferences(UserItem.PREFERENCIAS_DE_USUARIO, Context.MODE_PRIVATE);
        editor_user = prefs_user.edit();

        // Initializing
        mTitle = mDrawerTitle = getTitle();
        nameServer = (String) mTitle;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        firstDownload = false;
        if (savedInstanceState == null) {
            firstDownload = true;
            inFavorites = true;
        }

        getListServers = new GetListServers(mDrawerList, this);
        //getListServers.execute();
        getListServers.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        //getActionBar().setIcon(R.drawable.mangayomu_neg);
        getActionBar().setIcon(R.drawable.mangayomu_neg);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.drawer_open,
                R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
                getActionBar().setTitle(nameServer);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            editor_user.putBoolean("APLICACION_ABIERTA", true);
            editor_user.commit();
            SelectItem(INDICE_PERSONAL+1);
        }

        if(nameServer != null) setTitle(nameServer);

        System.out.println("onCreate: nameServer = " + nameServer + ", serverSelected = ");

        Time time = new Time();
        time.setToNow();
        currentTime = time.toMillis(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("ROTATION","MainActivity: onCreateOptionsMenu");
        System.out.println("Entro en menú");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_buscar).getActionView();
        if(searchView != null){
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setQueryHint("Buscar aquí");
        }

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                System.out.println("onQueryTextSubmit: " + s);
                if(FragmentCenter.adapter != null){
                    FragmentCenter.adapter.getFilter().filter(s);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                System.out.println("onQueryTextChange: " + s);
                if(FragmentCenter.adapter != null){
                    FragmentCenter.adapter.getFilter().filter(s);
                }
                return false;
            }
        };

        System.out.println("Creando menú");
        searchView.setOnQueryTextListener(queryTextListener);
        System.out.println("Menú creado");

        //System.out.println("En el menú de MainActivity, searchView = " + searchView + ", " + searchView.getQuery());


        System.out.println("Salgo del menú");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("ROTATION","MainActivity: onOptionsItemSelected");
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()){
            case R.id.menu_preferencias:
                Toast.makeText(this, "Preferencias", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_buscar:
                break;
            case R.id.sincronizar:
                if(fragmentFavourite != null)
                    fragmentFavourite.sincronizar();
                else {
                    if(prefs_user.getString("USER_NAME", null) != null){
                        Toast.makeText(actividad, "Ha fallado la sincronización, vuelve a darle a favoritos", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(actividad, "No estás logueado", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("ROTATION","MainActivity: onConfigurationChanged");
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void SelectItem(int position) {

        if(position == INDICE_SESION){
            if(prefs_user.getString("USER_NAME", null) != null){
                Cerrar_sesion();
            }
            else {
                Iniciar_Sesion("Iniciar sesión");
            }
            mDrawerLayout.closeDrawer(mDrawerList); // cerramos el menú lateral
        }
        else if(position == INDICE_PERSONAL + 1){
            nameServer = "Favoritos";
            inFavorites = true;
            editor.putString(ITEM_NAME_SERVER, nameServer);
            editor.commit();

            fragmentFavourite = new FragmentFavourites();

            FragmentManager frgManager = getFragmentManager();
            frgManager.beginTransaction().replace(R.id.content_frame, fragmentFavourite).commit();

            mDrawerList.setItemChecked(position, true); // remarcar el elemento x de la lista, definido con possition
            mDrawerLayout.closeDrawer(mDrawerList); // cerramos el menú lateral
        }
        else if(position > INDICE_SERVIDORES){
            inFavorites = false;
            nameServer = servers.getList_servers().get(position-(INDICE_SERVIDORES+1));
            editor.putString(ITEM_NAME_SERVER, nameServer);
            editor.commit();

            Fragment fragment = null;
            Bundle args = new Bundle();
            fragment = new FragmentCenter(searchView);
            args.putString(FragmentCenter.ITEM_NAME_SERVER, nameServer);

            fragment.setArguments(args);
            fragment.setRetainInstance(true);
            FragmentManager frgManager = getFragmentManager();
            frgManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerList.setItemChecked(position, true); // remarcar el elemento x de la lista, definido con possition
            mDrawerLayout.closeDrawer(mDrawerList); // cerramos el menú lateral
        }
        else if(position == INDICE_PERSONAL + 2 || position == INDICE_PERSONAL + 3){
            Toast.makeText(this, "No implementado", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onItemCenterSelecter(int position, String nameManga) {
        //Toast.makeText(this,"Servidor " + mTitle + ", manga " + nameManga, Toast.LENGTH_LONG).show();
        Intent activity = new Intent(this, MangaActivity.class);
        System.out.println("nameServer = " + nameServer);
        activity.putExtra(ITEM_NAME_SERVER, nameServer);
        activity.putExtra(ITEM_NAME_MANGA, nameManga);
        //activity.putExtra(CLASIFICATION_MANGA, position+1);

        editor.putString(ITEM_NAME_MANGA, nameManga);
        editor.putInt(MangaActivity.POSITION_VIEWPAGER_MANGA, 0);
        //editor.putInt(CLASIFICATION_MANGA, position+1);
        editor.commit();

        //db.setClasificationManga(nameServer, nameManga, position+1);

        startActivity(activity);
    }

    @Override
    public void onItemFavouriteSelecter(MangaItem mangaItem) {
        nameServer = mangaItem.getServer_name();

        Intent activity = new Intent(this, MangaActivity.class);

        activity.putExtra(ITEM_NAME_SERVER, nameServer);
        activity.putExtra(ITEM_NAME_MANGA, mangaItem.getName());
        //activity.putExtra(CLASIFICATION_MANGA, clasification);

        editor.putString(ITEM_NAME_MANGA, mangaItem.getName());
        editor.putInt(MangaActivity.POSITION_VIEWPAGER_MANGA, 0);
        //editor.putInt(CLASIFICATION_MANGA, clasification);
        editor.commit();

        startActivity(activity);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(!inFavorites){
            inFavorites = true;
            editor.putString(ITEM_NAME_SERVER, "Favoritos");
            editor.commit();

            fragmentFavourite = new FragmentFavourites();

            FragmentManager frgManager = getFragmentManager();
            frgManager.beginTransaction().replace(R.id.content_frame, fragmentFavourite).commit();

            mDrawerList.setItemChecked(INDICE_PERSONAL+1, true); // remarcar Favoritos en la lista
        }
        else{
            Time time = new Time();
            time.setToNow();
            long ct = time.toMillis(false);
            if( (ct - currentTime) < TIME_TO_CLOSE_APP ){
                finish();
            }
            else{
                currentTime = time.toMillis(false);
                Toast.makeText(this, "Pulsa el botón Atrás para salir de la aplicación!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Log.d("ROTATION","MainActivity: onPostCreate");
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            SelectItem(position);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("ROTATION","MainActivity: onSaveInstanceState");
        //outState.put
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ROTATION","MainActivity: onResume");
        System.out.println("onResume from MainActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("ROTATION","MainActivity: onPause");

        if(dialog_inicio_sesion != null){
            dialog_inicio_sesion.dismiss();
        }
        if(dialog_cerrar_sesion != null){
            dialog_cerrar_sesion.dismiss();
        }
        if(dialog_testLogin != null){
            dialog_testLogin.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("ROTATION","MainActivity: onDestroy");
        //editor.putString(ITEM_NAME_SERVER, null);
        //editor.commit();
        System.out.println("onDestroy from MainActivity");
    }


    class GetListServers extends AsyncTask<Void, Void, String>{
        ListView listView;
        Activity actividad;
        Vector<DrawerItem> drawerItemList;

        GetListServers(ListView listView, Activity activity){
            this.listView = listView;
            this.actividad = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            db = new DataBase(actividad);
            servers = db.getListServers();

            drawerItemList = new Vector<DrawerItem>();

            drawerItemList.add(new DrawerItem("", false));
            drawerItemList.add(new DrawerItem("Personal", true));
            drawerItemList.add(new DrawerItem("Favoritos", false));
            drawerItemList.add(new DrawerItem("Descargados", false));
            drawerItemList.add(new DrawerItem("Amigos", false));
            drawerItemList.add(new DrawerItem("Fuente de servidores", true));

            if(servers != null) {
                for(String ns : servers.getList_servers()){
                    drawerItemList.add(new DrawerItem(ns, false));
                }
            }

            adapter = new CustomDrawerAdapter(actividad, R.layout.custom_drawer_item, drawerItemList);
            listView.setAdapter(adapter);

            adapter.setStateProgressBar(ProgressBar.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... voids) {
            if(firstDownload) {
                System.out.println("###################### firstDownload is true");
                if (!Utils.verificaConexion(actividad)) {
                    cancel(true);
                    return null;
                } else {
                    String responseString = Utils.obtenerListaServidores();
                    return responseString;
                }
            }
            else{
                System.out.println("###################### firstDownload is false");
                return null;
            }
        }

        @Override
        protected void onPostExecute(String jsonString) {
            if (jsonString != null){
                ServersItem newServers = new ServersItem();
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                newServers = gson.fromJson(jsonString, ServersItem.class);
                if(newServers != null){
                    shuffle_and_store_in_database(servers, newServers);
                }
            }
            System.out.println("###################### OCULTAMOS EL PROGRESSBAR");
            adapter.setStateProgressBar(ProgressBar.INVISIBLE);
        }

        @Override
        protected void onCancelled(String s) {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            adapter.setStateProgressBar(ProgressBar.INVISIBLE);
        }

        private void shuffle_and_store_in_database(ServersItem _old, ServersItem _new){
            if(_old == null || _old.getList_servers() == null){ // la base de datos está vacía, por lo tanto guardamos directamente los descargados del servidor
                if(_new != null) {
                    for(String ns : _new.getList_servers()){
                        drawerItemList.add(new DrawerItem(ns, false));
                    }
                    servers = _new;
                    db.addServers(_new); // guardamos en la base de datos
                    adapter.setDrawerItemList(drawerItemList); // actualizamos la lista de servidores
                }
            }
            else if(_new != null && _new.getList_servers() != null) { // la base de datos no está vacía, por lo que hay que barajar y añadir y/o quitar los servidores correspondientes
                Vector<String> toInsert = new Vector<String>();
                Vector<String> toRemove = _old.getList_servers();

                for (String ns : _new.getList_servers()) {
                    int i = toRemove.indexOf(ns);
                    if(i != -1){
                        toRemove.remove(i);
                        System.out.println("Encontrado: " + ns);
                    }
                    else{
                        toInsert.add(ns);
                        System.out.println("A añadir: " + ns);
                    }
                }

                for(String trm : toRemove){
                    System.out.println("A eliminar: " + trm);
                }
                db.removeServers(toRemove);
                db.addServers(toInsert);

                servers = db.getListServers();

                drawerItemList = new Vector<DrawerItem>();
                drawerItemList.add(new DrawerItem("", false));
                drawerItemList.add(new DrawerItem("Personal", true));
                drawerItemList.add(new DrawerItem("Favoritos", false));
                drawerItemList.add(new DrawerItem("Descargados", false));
                drawerItemList.add(new DrawerItem("Amigos", false));
                drawerItemList.add(new DrawerItem("Fuente de servidores", true));

                if(servers != null) {
                    for(String ns : servers.getList_servers()){
                        drawerItemList.add(new DrawerItem(ns, false));
                    }
                }

                adapter.setDrawerItemList(drawerItemList);
            }
        }
    }


    void Iniciar_Sesion(String message){

        // Diálogo para introducir los datos de la sesión, usuario y email
        alert_inicio_sesion = new AlertDialog.Builder(actividad);
        alert_inicio_sesion.setTitle(message);
        alert_inicio_sesion.setCancelable(false);

        // Create LinearLayout to put into the dialog_inicio_sesion
        LinearLayout layout = new LinearLayout(actividad);
        layout.setOrientation(LinearLayout.VERTICAL);

        // Create textbox to put into the layout
        final EditText input_mail = new EditText(actividad);
        input_mail.setHint("Correo");
        layout.addView(input_mail);

        // Add layout to the dialog_inicio_sesion
        alert_inicio_sesion.setView(layout);

        // procedure for when the ok button is clicked.
        alert_inicio_sesion.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value_mail;
                value_mail = input_mail.getText().toString();
                if(Validar.Mail(value_mail)){
                    editor_user.putString("MAIL", value_mail);
                    editor_user.commit();
                    new IniciarSesion(null, value_mail).execute();
                    dialog.dismiss();
                }
                else{
                    dialog.dismiss();
                    Iniciar_Sesion("Iniciar sesión - Formato introducido erroneo");
                }

                return;
            }
        });

        alert_inicio_sesion.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });
        alert_inicio_sesion.show();
    }

    void Registrar_Cuenta(String mensaje){

        // Diálogo para introducir los datos de la sesión, usuario y email
        alert_inicio_sesion = new AlertDialog.Builder(actividad);
        alert_inicio_sesion.setTitle(mensaje);
        alert_inicio_sesion.setCancelable(false);

        // Create LinearLayout to put into the dialog_inicio_sesion
        LinearLayout layout = new LinearLayout(actividad);
        layout.setOrientation(LinearLayout.VERTICAL);

        // Create textbox to put into the layout
        final EditText input_user = new EditText(actividad);
        input_user.setHint("Usuario");
        layout.addView(input_user);
        // Create textbox to put into the layout
        final EditText input_mail = new EditText(actividad);
        String mail = prefs_user.getString("MAIL", null);
        if(mail != null) {
            input_mail.setText(mail);
            input_mail.setEnabled(false);
        }
        else
            input_mail.setHint("Correo");
        layout.addView(input_mail);

        // Add layout to the dialog_inicio_sesion
        alert_inicio_sesion.setView(layout);

        // procedure for when the ok button is clicked.
        alert_inicio_sesion.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value_user, value_mail;
                value_user = input_user.getText().toString();
                value_mail = input_mail.getText().toString();
                if(Validar.User(value_user)){
                    new IniciarSesion(value_user, value_mail).execute();
                    dialog.dismiss();
                }
                else{
                    dialog.dismiss();
                    Registrar_Cuenta("Registro - El formato del nombre ha de ser alfanumérico");
                }
                return;
            }
        });

        alert_inicio_sesion.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });
        alert_inicio_sesion.show();
    }

    void Confirmar_PIN(final String user_name, String mensaje){

        alert_introducir_pin = new AlertDialog.Builder(actividad);
        alert_introducir_pin.setCancelable(false);
        alert_introducir_pin.setTitle(mensaje);
        alert_introducir_pin.setMessage("PIN:");
        // Create textbox to put into the dialog_inicio_sesion
        final EditText input = new EditText(actividad);
        // put the textbox into the dialog_inicio_sesion
        alert_introducir_pin.setView(input);
        // procedure for when the ok button is clicked.
        alert_introducir_pin.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String pin = input.getText().toString().trim();
                dialog.dismiss();
                editor_user.putString("USER_NAME", user_name);
                editor_user.putString("PIN", pin);
                editor_user.putBoolean("PRIMERA_CONEXION", true);
                editor_user.commit();
                // Comprobamos que estemos bien logeados
                new TestLogin().execute();
                return;
            }
        });
        alert_introducir_pin.show();

    }

    class IniciarSesion extends AsyncTask<Void, Void, HttpResponse>{

        String value_user, value_mail;

        public IniciarSesion(String value_user, String value_mail){
            this.value_user = value_user;
            this.value_mail = value_mail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Diálogo de espera para esperar a que el servidor responda
            dialog_inicio_sesion = new ProgressDialog(actividad);
            dialog_inicio_sesion.setMessage("Iniciando sesión");
            dialog_inicio_sesion.setCancelable(false);
            dialog_inicio_sesion.show();
        }

        @Override
        protected HttpResponse doInBackground(Void... voids) {

            if (!Utils.verificaConexion(actividad)) {
                cancel(true);
                return null;
            } else {
                HttpResponse response = Utils.logearse(value_user, value_mail);
                return response;
            }

        }

        @Override
        protected void onCancelled() {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            dialog_inicio_sesion.dismiss();
        }

        @Override
        protected void onPostExecute(HttpResponse response) {
            dialog_inicio_sesion.dismiss();
            int codigo = response.getStatusLine().getStatusCode();

            if(codigo == 200) {

                String jsonString = Utils.responseToString(response);

                if (jsonString != null) {
                    UserItem userItem = new UserItem();
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    userItem = gson.fromJson(jsonString, UserItem.class);

                    //editor_user.putString("USER_NAME", userItem.getUser_name());
                    final String user_name = userItem.getUser_name();
                    editor_user.putString("CLIENT_ID", userItem.getClient_id());
                    editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                    editor_user.commit();

                    Log.d("DEBUG", "logedado");
                    Log.d("DEBUG", "user_name: " + userItem.getUser_name());
                    Log.d("DEBUG", "client_id: " + userItem.getClient_id());
                    Log.d("DEBUG", "next_random: " + userItem.getNext_random());

                    intentos_introducir_pin = 1;
                    Confirmar_PIN(user_name, "Introduce el PIN que se ha enviado a tu correo, puede que esté en spam");
                }
            }
            else if(codigo == 400){
                // El usuario no existe, debe especificarse también el nombre de usuario
                Registrar_Cuenta("El mail no existe, añadir nombre de usuario para registrar cuenta");
            }
            else if(codigo == 403){
                // Ya existe otro usuario con ese nombre, debe elegirse otro
                Registrar_Cuenta("Ya existe otro usuario con ese nombre, debe elegirse otro");
            }
            else if(codigo == 500){
                // No se ha cumplido con el formato de mail o nombre de usuario
                Toast.makeText(actividad, "Formato erroneo", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void Cerrar_sesion(){

        /*
        editor_user.putString("USER_NAME", null);
        editor_user.putString("CLIENT_ID", null);
        editor_user.putString("NEXT_RANDOM", null);
        editor_user.putString("PIN", null);
        editor_user.commit();
        */
        /*editor_user.putString("CLIENT_ID", "54785ed51053ba4835000125");
        editor_user.commit();*/

        Log.d("DEBUG", "Cerrar sesión con los siguientes datos");
        Log.d("DEBUG", "user_name: " + prefs_user.getString("USER_NAME", null));
        Log.d("DEBUG", "client_id: " + prefs_user.getString("CLIENT_ID", null));
        Log.d("DEBUG", "next_random: " + prefs_user.getString("NEXT_RANDOM", null));
        Log.d("DEBUG", "pin: " + prefs_user.getString("PIN", null));

        Log.d("DEBUG", "key: " + Hash.md5(prefs_user.getString("PIN", null) + prefs_user.getString("NEXT_RANDOM", null)));

        // Diálogo para introducir los datos de la sesión, usuario y email
        alert_cerrar_sesion = new AlertDialog.Builder(actividad);
        alert_cerrar_sesion.setTitle("Cerrar sesión");
        alert_cerrar_sesion.setMessage("¿Quiere cerrar sesión?");

        // procedure for when the ok button is clicked.
        alert_cerrar_sesion.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                new CerrarSesion().execute();
                dialog.dismiss();
                return;
            }
        });

        alert_cerrar_sesion.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });
        alert_cerrar_sesion.show();
    }

    class CerrarSesion extends AsyncTask<Void, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Diálogo de espera para esperar a que el servidor responda
            dialog_cerrar_sesion = new ProgressDialog(actividad);
            dialog_cerrar_sesion.setMessage("Cerrando sesión");
            dialog_cerrar_sesion.setCancelable(false);
            dialog_cerrar_sesion.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            if (!Utils.verificaConexion(actividad)) {
                cancel(true);
                return null;
            } else {
                String client_id = prefs_user.getString("CLIENT_ID", null);
                String pin = prefs_user.getString("PIN", null);
                String next_random = prefs_user.getString("NEXT_RANDOM", null);
                String key = Hash.md5(pin+next_random);
                String responseString = Utils.logout(client_id, key);
                return responseString;
            }

        }

        @Override
        protected void onCancelled() {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            dialog_cerrar_sesion.dismiss();
        }

        @Override
        protected void onPostExecute(String jsonString) {
            dialog_cerrar_sesion.dismiss();

            Log.d("DEBUG", "userItem is : " + jsonString);

            if(jsonString != null) {
                UserItem userItem = new UserItem();
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                try {
                    userItem = gson.fromJson(jsonString, UserItem.class);
                } catch (Exception e){
                    e.printStackTrace();
                }
                final String mensajeDesconexionExito;
                boolean desconexion_fallida;

                mensajeDesconexionExito = userItem.getUser_name()+", ha sido deslogeado con éxito";

                if(userItem.getUser_name() == null){
                    editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                    editor_user.commit();
                    desconexion_fallida = true;
                }
                else {
                    editor_user.putString("USER_NAME", null);
                    editor_user.putString("CLIENT_ID", null);
                    editor_user.putString("NEXT_RANDOM", null);
                    editor_user.putString("PIN", null);
                    editor_user.commit();
                    desconexion_fallida = false;
                }

                if(desconexion_fallida){
                    alert_desconexion_fallida = new AlertDialog.Builder(actividad);
                    alert_desconexion_fallida.setTitle("La desconexión ha fallado");
                    alert_desconexion_fallida.setMessage("¿Quiere forzar el cierre de sesión?");

                    // procedure for when the ok button is clicked.
                    alert_desconexion_fallida.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            editor_user.putString("USER_NAME", null);
                            editor_user.putString("CLIENT_ID", null);
                            editor_user.putString("NEXT_RANDOM", null);
                            editor_user.putString("PIN", null);
                            editor_user.commit();

                            alert_desconexion = new AlertDialog.Builder(actividad);
                            alert_desconexion.setTitle(mensajeDesconexionExito);
                            // procedure for when the ok button is clicked.
                            alert_desconexion.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    return;
                                }
                            });
                            alert_desconexion.show();
                            // Para que se vuelva a ver Login en el menú
                            adapter.updateChanges();
                            return;
                        }
                    });

                    alert_desconexion_fallida.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
                    alert_desconexion_fallida.show();
                }
                else{
                    alert_desconexion = new AlertDialog.Builder(actividad);
                    alert_desconexion.setTitle(mensajeDesconexionExito);

                    // procedure for when the ok button is clicked.
                    alert_desconexion.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            return;
                        }
                    });
                    alert_desconexion.show();
                    // Para que se vuelva a ver Login en el menú
                    adapter.updateChanges();
                }

            }

        }
    }

    class TestLogin extends AsyncTask<Void, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Diálogo de espera para esperar a que el servidor responda
            dialog_testLogin = new ProgressDialog(actividad);
            dialog_testLogin.setMessage("Comprobando inicio de sesión");
            dialog_testLogin.setCancelable(false);
            dialog_testLogin.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            if (!Utils.verificaConexion(actividad)) {
                cancel(true);
                return null;
            } else {
                String client_id = prefs_user.getString("CLIENT_ID", null);
                String pin = prefs_user.getString("PIN", null);
                String next_random = prefs_user.getString("NEXT_RANDOM", null);
                String key = Hash.md5(pin+next_random);
                String responseString = Utils.testLogin(client_id, key);
                return responseString;
            }

        }

        @Override
        protected void onCancelled() {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            dialog_testLogin.dismiss();
        }

        @Override
        protected void onPostExecute(String jsonString) {
            dialog_testLogin.dismiss();

            Log.d("DEBUG", "userItem in testLogin is : " + jsonString);

            if(jsonString != null) {
                UserItem userItem = new UserItem();
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                try {
                    userItem = gson.fromJson(jsonString, UserItem.class);
                } catch (Exception e){
                    e.printStackTrace();
                }
                String mensaje;

                if(userItem.getUser_name() == null){
                    String user_name = prefs_user.getString("USER_NAME", null);

                    editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                    editor_user.putString("USER_NAME", null);
                    editor_user.commit();

                   if(intentos_introducir_pin < maximo_intentos_introducir_pin) {
                       mensaje = "El pin no es correcto, inténtelo de nuevo";
                       Confirmar_PIN(user_name, mensaje);
                       intentos_introducir_pin++;
                   }
                    else{
                       mensaje = "Se han consumido "+ maximo_intentos_introducir_pin +" intentos, inténtelo de nuevo";
                       alert_login_correcto = new AlertDialog.Builder(actividad);
                       alert_login_correcto.setTitle(mensaje);

                       // procedure for when the ok button is clicked.
                       alert_login_correcto.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int whichButton) {
                               return;
                           }
                       });
                       alert_login_correcto.show();
                   }
                }
                else {
                    editor_user.putString("USER_NAME", userItem.getUser_name());
                    editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                    editor_user.commit();
                    mensaje = userItem.getUser_name()+", se ha logeado con éxito";

                    alert_login_correcto = new AlertDialog.Builder(actividad);
                    alert_login_correcto.setTitle(mensaje);

                    // procedure for when the ok button is clicked.
                    alert_login_correcto.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (prefs_user.getBoolean("PRIMERA_CONEXION", false)) {
                                fragmentFavourite.sincronizar();
                            }
                            return;
                        }
                    });
                    alert_login_correcto.show();
                }
            }

        }
    }

}
