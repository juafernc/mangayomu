package com.example.juanma.mangayomu;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Adapters.ChapterPagerAdapter;
import Fragments.FragmentChapter;
import Items.ChapterItem;
import dataBase.DataBase;
import red.ImageDownloaderCache;
import red.Utils;

/**
 * Created by juanma on 24/07/14.
 */
public class CapitulosActivity extends Activity implements ActionBar.TabListener{
    private static final String ITEM_NAME_SERVER = "item_name_server";
    private static final String ITEM_NAME_MANGA = "item_name_manga";
    private static final String ITEM_NAME_CHAPTER = "item_name_chapter";

    private static final String POSITION_VIEWPAGER_CHAPTER = "position_viewpager_chapter";

    ChapterPagerAdapter chapterPagerAdapter;

    public static ViewPager mViewPager;

    public static String nameServer;
    public static String nameManga;
    public static String nameChapter;

    private Activity actividad = this;

    public static ChapterItem chapter;
    public static int uiOption;

    ImageDownloaderCache downloaderCache;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    GetChapter getChapter;
    ProgressDialog dialog;

    DataBase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capitulos);

        aplicarFullScreen();

        prefs = getSharedPreferences("PreferenciasMangaYomu", Context.MODE_PRIVATE);
        editor = prefs.edit();

        Bundle args = getIntent().getExtras();
        nameServer = args.getString(ITEM_NAME_SERVER);
        nameManga = args.getString(ITEM_NAME_MANGA);
        nameChapter = args.getString(ITEM_NAME_CHAPTER);
        if(nameServer == null) nameServer = prefs.getString(ITEM_NAME_SERVER,null);
        if(nameManga == null) nameManga = prefs.getString(ITEM_NAME_MANGA,null);
        if(nameChapter == null) nameChapter = prefs.getString(ITEM_NAME_CHAPTER,null);

        downloaderCache = new ImageDownloaderCache(this.getCacheDir());

        //Toast.makeText(this, nameServer+", "+nameManga+", "+nameChapter, Toast.LENGTH_LONG).show();

        getChapter = new GetChapter();
        getChapter.execute();

    }

    /*@Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        super.onCreateView(name, context, attrs);
        swipeRefreshLayout = new SwipeRefreshLayout(this);
        return swipeRefreshLayout;
    }*/

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
        Log.d("DEBUG_TAB","onTabSelected");
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        Log.d("DEBUG_TAB","onTabUnselected");
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        Log.d("DEBUG_TAB","onTabReselected");
    }

    void aplicarFullScreen(){
        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;

        boolean isImmersiveModeEnabled = ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);

        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        uiOption = newUiOptions;
        //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        //getWindow().getDecorView().setSystemUiVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //editor.putInt(POSITION_VIEWPAGER_CHAPTER, mViewPager.getCurrentItem());
        //editor.commit();
        if(mViewPager != null) {
            System.out.println("onPause CapitulosActivity, getCurrentItem = " + String.valueOf(mViewPager.getCurrentItem()));
        }
        if(dialog != null){
            dialog.dismiss();
        }
        if(getChapter != null){
            getChapter.cancel(true);
        }
        if(downloaderCache != null) {
            downloaderCache.cancellTask();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    class GetChapter extends AsyncTask<Void, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialog_inicio_sesion = ProgressDialog.show(actividad, "", "Cargando " + nameChapter, true);
            dialog = new ProgressDialog(actividad);
            dialog.setMessage("Cargando " + nameChapter);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            db = new DataBase(actividad);
            chapter = db.getChapter(nameServer, nameManga, nameChapter);
            if(chapter != null){
                System.out.println("El capítulo no existe en la base de datos");
                return null;
            }
            else{
                if(!Utils.verificaConexion(actividad)){
                    cancel(true);
                    return null;
                }
                else {
                    String responseString = Utils.obtenerCapitulosManga(nameServer, nameManga, nameChapter);
                    return responseString;
                }
            }
        }

        @Override
        protected void onPostExecute(String jsonString) {
            if (jsonString != null) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                chapter = gson.fromJson(jsonString, ChapterItem.class);

                System.out.println("Descargado de internet");
                Toast.makeText(actividad, "Descargado de internet", Toast.LENGTH_LONG).show();

                if(chapter != null){
                    System.out.println("Añadiendo capítulo a la base de datos");
                    db.addChapter(chapter, nameServer, nameManga, nameChapter);
                }
            }

            if(chapter != null){
                mViewPager = (ViewPager) findViewById(R.id.pagerChapter);

                chapterPagerAdapter = new ChapterPagerAdapter(getFragmentManager(), actividad);
                for(int position = 0; position < chapter.getList_images().size(); position++) {
                    chapterPagerAdapter.addFragment(FragmentChapter.newInstance(position));
                }
                //System.out.println("onCreate CapitulosActivity");
                mViewPager.setAdapter(chapterPagerAdapter);
                /*mViewPager.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_MOVE:
                                //Toast.makeText(actividad, "ACTION_MOVE", Toast.LENGTH_SHORT).show();
                                Log.d("SWING", "ACTION_MOVE");
                                swipeRefreshLayout.setEnabled(false);
                                break;
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL:
                                //Toast.makeText(actividad, "ACTION_UP_CANCEL", Toast.LENGTH_SHORT).show();
                                Log.d("SWING", "ACTION_UP_CANCEL");
                                swipeRefreshLayout.setEnabled(true);
                                break;
                        }
                        return false;
                    }
                });*/

                downloaderCache.download_to_cache(mViewPager, chapterPagerAdapter, chapter.getList_images(), nameServer, nameManga, nameChapter);
            }
            else{
                Toast.makeText(actividad, "No se pudo cargar el capítulo " + nameChapter, Toast.LENGTH_LONG).show();
                try {
                    actividad.finish();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

            dialog.dismiss();
        }

        @Override
        protected void onCancelled(String s) {
            dialog.dismiss();
            try {
                actividad.finish();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
        }
    }
}
