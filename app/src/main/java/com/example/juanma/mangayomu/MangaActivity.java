package com.example.juanma.mangayomu;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;
import java.util.Vector;

import Adapters.ChapterListAdapter;
import Adapters.MangaPagerAdapter;
import Fragments.FragmentMangaInfo;
import Fragments.FragmentMangaListChapters;
import Items.DrawerItem;
import Items.MangaItem;
import Items.ServersItem;
import Items.UserItem;
import dataBase.DataBase;
import red.Utils;
import util.Hash;

/**
 * Created by juanma on 22/07/14.
 */
public class MangaActivity extends Activity implements ActionBar.TabListener, FragmentMangaListChapters.OnListChaptersViewListener{

    private static final String ITEM_NAME_SERVER = "item_name_server";
    private static final String ITEM_NAME_MANGA = "item_name_manga";
    private static final String ITEM_NAME_CHAPTER = "item_name_chapter";
    private static final String CLASIFICATION_MANGA = "clasification_manga";

    public static final String POSITION_VIEWPAGER_MANGA = "position_viewpager_manga";

    MangaPagerAdapter mangaPagerAdapter;
    LinearLayout barraInferiorManga;
    ViewPager mViewPager;

    private String nameServer;
    private String nameManga;
    //public static Integer clasification;

    private Activity actividad = this;

    public static MangaItem manga;

    private CharSequence mTitle;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    SharedPreferences prefs_user;
    SharedPreferences.Editor editor_user;

    GetManga getManga;
    ProgressDialog dialog;

    DataBase db;

    boolean firstDownload;

    private MenuItem menuItem;
    ChapterListAdapter chapterListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manga);

        prefs = getSharedPreferences("PreferenciasMangaYomu", Context.MODE_PRIVATE);
        editor = prefs.edit();

        prefs_user = getSharedPreferences(UserItem.PREFERENCIAS_DE_USUARIO, Context.MODE_PRIVATE);
        editor_user = prefs_user.edit();

        Bundle args = getIntent().getExtras();
        nameServer = args.getString(ITEM_NAME_SERVER);
        nameManga = args.getString(ITEM_NAME_MANGA);
        //clasification = args.getInt(CLASIFICATION_MANGA);
        if(nameServer == null || nameServer.equals("Mangayomu")) nameServer = prefs.getString(ITEM_NAME_SERVER,null);
        if(nameManga == null) nameManga = prefs.getString(ITEM_NAME_MANGA,null);
        //if(clasification == null) clasification = prefs.getInt(CLASIFICATION_MANGA, -1);

        setTitle(nameManga);

        firstDownload = false;
        if (savedInstanceState == null) {
            firstDownload = true;
        }

        Log.d("DEBUG_NOF","firstDownload: " + firstDownload);

        // Activo el botón del actionbar de arriba a la izquierda para retroceder a la activity padre
        getActionBar().setDisplayHomeAsUpEnabled(true);

        getActionBar().setIcon(R.drawable.mangayomu_neg);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        barraInferiorManga = (LinearLayout) findViewById(R.id.barraInferiorManga);

        getManga = new GetManga(this, actionBar);
        getManga.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onItemChapterSelecter(int position) {
        //Toast.makeText(this, "Seleccionado elemento " + String.valueOf(position), Toast.LENGTH_SHORT).show();
        //Intent activity = new Intent(this, FullScreen.class);
        //Intent activity = new Intent(this, ChaptersActivity.class);

        Intent activity = new Intent(this, CapitulosActivity.class);

        activity.putExtra(ITEM_NAME_SERVER, nameServer);
        activity.putExtra(ITEM_NAME_MANGA, nameManga);
        Log.d("DEBUG_NOF", "MangaActivity->manga: " + manga);
        if(manga != null){
            Log.d("DEBUG_NOF", "position: " + position);
            Log.d("DEBUG_NOF", "MangaActivity->manga->list_chaters: "+ manga.getList_chapters());
            Log.d("DEBUG_NOF", "MangaActivity->manga->list_chaters.get("+position+"): "+ manga.getList_chapters().get(position));
        }
        activity.putExtra(ITEM_NAME_CHAPTER, manga.getList_chapters().get(position));

        editor.putString(ITEM_NAME_CHAPTER, manga.getList_chapters().get(position));
        editor.commit();

        startActivity(activity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.manga, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case R.id.actualizar_manga:
                Toast.makeText(this, "Actualizar", Toast.LENGTH_SHORT).show();
                //item.setEnabled(false);
                UpdateManga updateManga = new UpdateManga(this);
                updateManga.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                menuItem = item;
                menuItem.setActionView(R.layout.progress_bar);
                //menuItem.expandActionView();
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        menuItem.collapseActionView();
                        menuItem.setActionView(null);
                    }
                }.execute();

                break;

            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /*
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mangaPagerAdapter = (MangaPagerAdapter) savedInstanceState.getSerializable("mangaPagerAdapter");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("mangaPagerAdapter", mangaPagerAdapter);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("DEBUG_onResume","MangaActivity -> onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("MangaActivity, onPause()");
        if(editor != null && mViewPager != null) {
            editor.putInt(POSITION_VIEWPAGER_MANGA, mViewPager.getCurrentItem());
            editor.commit();
        }
        else{
            System.out.println("El editor de MangaActivity es null");
        }
        if(dialog != null){
            dialog.dismiss();
        }
        if(getManga != null){
            getManga.cancel(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("MangaActivity onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //System.out.println("onDestroy() MangaActivity, page = " + mViewPager.getCurrentItem());
        System.out.println("MangaActivity onDestroy()");
        //manga = null;
        //editor.putString(ITEM_NAME_MANGA,null);
        //editor.commit();
    }

    class GetManga extends AsyncTask<Void, Void, String>{
        Activity activity;
        ActionBar actionBar;

        GetManga(Activity activity, ActionBar actionBar){
            this.activity = activity;
            this.actionBar = actionBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(actividad);
            dialog.setMessage("Cargando " + nameManga);
            dialog.setCancelable(true);
            dialog.show();

            db = new DataBase(activity);
            manga = db.getManga(nameServer, nameManga);

            Log.d("DEBUG_NOF","manga: " + manga);

            if(manga != null){ // si existe en la base de datos, creamos vista
                //Log.d("DEBUG_NOF","manga.getList_chapters(): " + manga.getList_chapters());
                createPagerView(actionBar, manga, false);
                dialog.dismiss();
            }
            else{
                Log.d("DEBUG_NOF","manga es null");
            }
        }

        @Override
        protected String doInBackground(Void... voids) {

            if(firstDownload) {
                if (!Utils.verificaConexion(actividad)) {
                    cancel(true);
                    return null;
                } else {
                    String responseString = Utils.obtenerManga(nameServer, nameManga);
                    Log.d("DEBUG_NOF","responseString: " + responseString);
                    return responseString;
                }
            }
            else{
                // No es la primera descarga
                return null;
            }

        }

        @Override
        protected void onPostExecute(String jsonString) {

            MangaItem newManga = new MangaItem();

            if (jsonString != null) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                newManga = gson.fromJson(jsonString, MangaItem.class);

                System.out.println("Descargado de internet");
                Toast.makeText(actividad, "Descargado de internet", Toast.LENGTH_LONG).show();

                if(newManga != null){
                    //System.out.println("Añadiendo manga a la base de datos");
                    shuffle_and_store_in_database(manga, newManga);
                }
            }

            if(manga == null && newManga == null){
                try {
                    Toast.makeText(activity, "No se pudo cargar el manga " + nameManga, Toast.LENGTH_LONG).show();
                    actividad.finish();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

            dialog.dismiss();
        }

        @Override
        protected void onCancelled(String s) {
            if(dialog.isShowing()){
                dialog.dismiss();
                try {
                    actividad.finish();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            }
        }

        private void shuffle_and_store_in_database(MangaItem _old, MangaItem _new){
            if(_old == null){ // la base de datos está vacía, por lo tanto guardamos directamente los descargados del servidor
                if(_new != null) {
                    manga = _new; // asignamos un valor a manga
                    db.addManga(manga); // guardamos en la base de datos
                    createPagerView(actionBar, manga, true); // creamos la vista del pager, ya que antes no existía
                }
            }
            else if(_new != null && _new.getList_chapters() != null && _new.getList_chapters().size() > 0) { // la base de datos no está vacía, por lo que hay que barajar y añadir y/o quitar los capítulos correspondientes

                if (_old.getList_chapters().indexOf(_new.getList_chapters().get(_new.getList_chapters().size()-1)) == -1) {
                    db.addManga(_new); // actualizamos el manga en la base de datos
                    Collections.reverse(_new.getList_chapters());
                    manga = _new; // actualizamos el objeto manga
                    ((FragmentMangaListChapters) mangaPagerAdapter.getFragments().get(1)).getAdapter().setMangaItem(manga); // actualizamos la lista del adapter
                }

            }
        }
    }


    void createPagerView(final ActionBar actionBar, final MangaItem mangaItem, boolean reverse){
        if(reverse) Collections.reverse(mangaItem.getList_chapters());

        if(mViewPager == null) {
            mViewPager = (ViewPager) findViewById(R.id.pagerManga);

            System.out.println("Creando PagerView");

            if(mangaPagerAdapter == null) {
                mangaPagerAdapter = new MangaPagerAdapter(getFragmentManager());
                mangaPagerAdapter.addFragment(FragmentMangaInfo.newInstance(mangaItem));
                //System.out.println("mangaItem: " + mangaItem + ", nameServer: " + nameServer + ", nameManga: " + nameManga);
                mangaPagerAdapter.addFragment(FragmentMangaListChapters.newInstance(mangaItem));
                mViewPager.setAdapter(mangaPagerAdapter);
            }
            //System.out.println("PagerView creado con id del fragment: " + ((FragmentMangaListChapters)mangaPagerAdapter.getFragments().get(1)).getVariable_random_debuger());
        }
        final ImageView startToRead, markToFavourite;
        startToRead = (ImageView) findViewById(R.id.startReadManga);
        markToFavourite = (ImageView) findViewById(R.id.markToFavourite);

        if(db.isMangaFavourite(nameServer, nameManga)){
            markToFavourite.setImageResource(android.R.drawable.btn_star_big_on);
        }
        else{
            markToFavourite.setImageResource(android.R.drawable.btn_star_big_off);
        }

        markToFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(db.isMangaFavourite(nameServer, nameManga)){
                    if(db.removeMangaFavourite(nameServer, nameManga)){
                        new UpdateFavouriteOnServer(nameServer, nameManga, false, markToFavourite).execute();
                    }
                }
                else{
                    if(db.putOnMangaFavourite(nameServer, nameManga)){
                        new UpdateFavouriteOnServer(nameServer, nameManga, true, markToFavourite).execute();
                    }
                }
            }
        });

        startToRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemChapterSelecter(mangaItem.getList_chapters().size()-1);
            }
        });

        // para marcar en que pestaña estoy
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // para añadir el tab en el action bar
        for (int i = 0; i < mangaPagerAdapter.getCount(); i++) {
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mangaPagerAdapter.getPageTitle(i))
                            .setTabListener(MangaActivity.this)
            );
        }

        mViewPager.setCurrentItem(prefs.getInt(POSITION_VIEWPAGER_MANGA, 0));

        barraInferiorManga.setVisibility(LinearLayout.VISIBLE);
    }

    class UpdateManga extends AsyncTask<Void, Void, String>{
        Activity activity;

        UpdateManga(Activity activity){
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... voids) {

            if (!Utils.verificaConexion(actividad)) {
                cancel(true);
                return null;
            } else {
                String responseString = Utils.obtenerManga(nameServer, nameManga);
                return responseString;
            }

        }

        @Override
        protected void onPostExecute(String jsonString) {

            MangaItem newManga = new MangaItem();

            if (jsonString != null) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                newManga = gson.fromJson(jsonString, MangaItem.class);

                System.out.println("Descargado de internet");
                Toast.makeText(actividad, "Descargado de internet", Toast.LENGTH_LONG).show();

                if(newManga != null && manga != null){
                    shuffle_and_store_in_database(manga, newManga);
                }
            }

        }

        @Override
        protected void onCancelled(String s) {
            Toast.makeText(actividad, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
        }

        private void shuffle_and_store_in_database(MangaItem _old, MangaItem _new){

            if(_new != null && _new.getList_chapters() != null && _new.getList_chapters().size() > 0) { // la base de datos no está vacía, por lo que hay que barajar y añadir y/o quitar los capítulos correspondientes

                if (_old.getList_chapters().indexOf(_new.getList_chapters().get(_new.getList_chapters().size()-1)) == -1) {
                    db.addManga(_new);
                    Collections.reverse(_new.getList_chapters());
                    manga = _new;
                    ((FragmentMangaListChapters) mangaPagerAdapter.getFragments().get(1)).getAdapter().setMangaItem(manga); // actializamos la lista del adapter
                }

            }
        }
    }

    class UpdateFavouriteOnServer extends AsyncTask<Void, Void, String>{

        String server_name;
        String manga_name;
        boolean insert;
        ImageView imageStar;

        UpdateFavouriteOnServer(String server_name, String manga_name, boolean insert, ImageView imageStar){
            this.server_name = server_name;
            this.manga_name = manga_name;
            this.insert = insert;
            this.imageStar = imageStar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Diálogo de espera para esperar a que el servidor responda
            /*dialog_inicio_sesion = new ProgressDialog(actividad);
            dialog_inicio_sesion.setMessage("Actualizando favoritos");
            dialog_inicio_sesion.setCancelable(false);
            dialog_inicio_sesion.show();*/

            imageStar.setImageResource(Color.TRANSPARENT);
            imageStar.setEnabled(false);
        }

        @Override
        protected String doInBackground(Void... voids) {

            if (!Utils.verificaConexion(actividad)) {
                cancel(true);
                return null;
            }
            else {
                String user_name = prefs_user.getString("USER_NAME", null);
                // Si estamos logueados
                if(user_name != null) {
                    String client_id = prefs_user.getString("CLIENT_ID", null);
                    String pin = prefs_user.getString("PIN", null);
                    String next_random = prefs_user.getString("NEXT_RANDOM", null);
                    String key = Hash.md5(pin + next_random);

                    String responseString;
                    if (insert) {
                        responseString = Utils.pushFavourite(
                                user_name,
                                client_id,
                                key,
                                server_name,
                                manga_name,
                                db.getListNamesChaptersReaded(server_name, manga_name)
                        );
                    } else {
                        responseString = Utils.removeFavourite(
                                user_name,
                                client_id,
                                key,
                                server_name,
                                manga_name
                        );
                    }

                    if (responseString != null) {
                        UserItem userItem = new UserItem();
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        try {
                            userItem = gson.fromJson(responseString, UserItem.class);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (userItem != null) {
                            editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                            editor_user.commit();
                        }

                    }
                }
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(actividad, "No hay conexión a internet, no se sincronizarán los cambios realizados", Toast.LENGTH_SHORT).show();
            if(insert){
                imageStar.setImageResource(android.R.drawable.btn_star_big_on);
            }
            else{
                imageStar.setImageResource(android.R.drawable.btn_star_big_off);
            }
            imageStar.setEnabled(true);
            //dialog_inicio_sesion.dismiss();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //dialog_inicio_sesion.dismiss();
            if(insert){
                imageStar.setImageResource(android.R.drawable.btn_star_big_on);
            }
            else{
                imageStar.setImageResource(android.R.drawable.btn_star_big_off);
            }
            imageStar.setEnabled(true);
        }

    }
}
