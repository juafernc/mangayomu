package dataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import Items.ChapterItem;
import Items.FavouriteItem;
import Items.MangaItem;
import Items.ServerItem;
import Items.ServersItem;

/**
 * Created by juanma on 8/08/14.
 */

/*
* Animextremist - Battle Royale
* Animextremist - Nisekoi
* MangaHere ES - Detective Conan
* Submanga - Fairy Tail
* Submanga - Kenichi
* Submanga . Naruto
* */

public class DataBase extends SQLiteOpenHelper {

    public DataBase(Context context) {
        super(context, "MangaYomuDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query;

        query = "CREATE TABLE server (name TEXT, object TEXT, PRIMARY KEY (name));";
        try{
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Fallo al crear la tabla server");
        }
        //query = "CREATE TABLE manga (name TEXT, server_name TEXT, object TEXT, favourite INTEGER, clasification INTEGER, PRIMARY KEY (name, server_name));";
        query = "CREATE TABLE manga (name TEXT, server_name TEXT, object TEXT, favourite INTEGER, PRIMARY KEY (name, server_name));";
        try{
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Fallo al crear la tabla manga");
        }
        query = "CREATE TABLE chapter (name TEXT, server_name TEXT, manga_name TEXT, object TEXT, readed INTEGER, PRIMARY KEY (name, server_name, manga_name));";
        try{
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Fallo al crear la tabla chapter");
        }
        //System.out.println("################################################################ onCreate DataBase ################################################################");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    public ServersItem getListServers(){
        ServersItem servers = new ServersItem();
        Vector<String> listServers = new Vector<String>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT name FROM server ORDER BY name";
        Cursor cursor = db.rawQuery(query, null);
        while(cursor.moveToNext()){
            listServers.add(cursor.getString(0));
        }
        cursor.close();
        if(listServers.size() == 0) return null;
        servers.setList_servers(listServers);
        return servers;
    }

    public ServerItem getListMangasFromServer(String nameServer){
        ServerItem server = null;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT object FROM server WHERE name LIKE '" + nameServer + "'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToNext()){
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            String jsonString = cursor.getString(0);
            if (jsonString != null) {
                server = gson.fromJson(jsonString, ServerItem.class);
            }
        }
        else{
            cursor.close();
            return null;
        }
        cursor.close();
        return server;
    }

    public MangaItem getManga(String nameServer, String nameManga){
        MangaItem manga = null;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT object FROM manga WHERE server_name LIKE '" + nameServer + "' and name LIKE '" + nameManga + "'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToNext()){
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            String jsonString = cursor.getString(0);
            if (jsonString != null) {
                manga = gson.fromJson(jsonString, MangaItem.class);
            }
        }
        else{
            cursor.close();
            return null;
        }
        cursor.close();
        Collections.reverse(manga.getList_chapters());
        return manga;
    }

    public ChapterItem getChapter(String nameServer, String nameManga, String nameChapter){
        ChapterItem chapter = null;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT object FROM chapter WHERE server_name LIKE '" + nameServer + "' and manga_name LIKE '" + nameManga + "' and name LIKE '" + nameChapter + "'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToNext()){
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            String jsonString = cursor.getString(0);
            if (jsonString != null) {
                chapter = gson.fromJson(jsonString, ChapterItem.class);
            }
        }
        else{
            cursor.close();
            return null;
        }
        cursor.close();
        return chapter;
    }

    public boolean addServers(ServersItem servers){
        SQLiteDatabase db = getWritableDatabase();
        String query;
        for(String nameServer : servers.getList_servers()) {
            query = "INSERT OR REPLACE INTO server(name) VALUES('" + nameServer + "'); ";
            try {
                db.execSQL(query);
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Ha fallado la insercción del servidor " + nameServer + " en la base de datos");
                return false;
            }
        }
        return true;
    }

    public boolean addServers(Vector<String> servers){
        SQLiteDatabase db = getWritableDatabase();
        String query;
        for(String nameServer : servers) {
            query = "INSERT OR REPLACE INTO server(name) VALUES('" + nameServer + "'); ";
            try {
                db.execSQL(query);
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Ha fallado la insercción del servidor " + nameServer + " en la base de datos");
                return false;
            }
        }
        return true;
    }

    public boolean removeServers(Vector<String> servers){
        SQLiteDatabase db = getWritableDatabase();
        String query;
        for(String nameServer : servers) {
            query = "DELETE FROM server WHERE name LIKE '"+nameServer+"'";
            try {
                db.execSQL(query);
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Ha fallado la eliminación del servidor " + nameServer + " en la base de datos");
                return false;
            }
        }
        return true;
    }


    public boolean addServer(ServerItem server){
        SQLiteDatabase db = getWritableDatabase();
        Gson gson = new Gson();
        String serverJson = gson.toJson(server);
        String query = "UPDATE server SET object = '" + serverJson + "' WHERE name LIKE '" + server.getName() + "'";
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean addManga(MangaItem manga){
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        Gson gson = new Gson();
        String mangaJson = gson.toJson(manga);
        //String query = "INSERT OR REPLACE INTO manga(name, server_name, object, favourite, clasification) VALUES('"+manga.getName()+"', '"+nameServer+"', '"+mangaJson+"', 0, "+clasification+")";
        String query = "INSERT OR REPLACE INTO manga(name, server_name, object, favourite) VALUES('"+manga.getName()+"', '"+manga.getServer_name()+"', '"+mangaJson+"', 0)";
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            db.setTransactionSuccessful();
            db.endTransaction();
            return false;
        }
        for(String nameChapter : manga.getList_chapters()){
            query = "INSERT OR REPLACE INTO chapter(name, server_name, manga_name, readed) VALUES('"+nameChapter+"', '"+manga.getServer_name()+"', '"+manga.getName()+"', 0)";
            try {
                db.execSQL(query);
            } catch (Exception e){
                e.printStackTrace();
                db.setTransactionSuccessful();
                db.endTransaction();
                return false;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    public boolean addNewChapters(MangaItem manga, Vector<String> newsChapters){
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        for(String nameChapter : newsChapters){
            String query = "INSERT OR REPLACE INTO chapter(name, server_name, manga_name, readed) VALUES('"+nameChapter+"', '"+manga.getServer_name()+"', '"+manga.getName()+"', 0);";
            try {
                db.execSQL(query);
                Log.d("Añadir","query newchapters: " + query);
            } catch (Exception e){
                e.printStackTrace();
                db.setTransactionSuccessful();
                db.endTransaction();
                return false;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return true;
    }

    public boolean addChapter(ChapterItem chapter, String nameServer, String nameManga, String nameChapter){
        SQLiteDatabase db = getWritableDatabase();
        Gson gson = new Gson();
        String chapterJson = gson.toJson(chapter);
        String query = "INSERT OR REPLACE INTO chapter(name, server_name, manga_name, object, readed) VALUES('"+nameChapter+"', '"+nameServer+"', '"+nameManga+"', '"+chapterJson+"', 0)";
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /*public int getClasificationManga(String nameServer, String nameManga){
        SQLiteDatabase db = getReadableDatabase();
        int clasification;
        String query = "SELECT clasification FROM manga WHERE server_name LIKE '" + nameServer + "' and name LIKE '" + nameManga + "'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToNext()){
            clasification = cursor.getInt(0);
        }
        else{
            cursor.close();
            return -1;
        }
        cursor.close();
        //System.out.println("Sacada clasificación de "+nameManga+", siendo = " + String.valueOf(clasification));
        return clasification;
    }*/

    /*public boolean setClasificationManga(String nameServer, String nameManga, int clasification){
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE manga SET clasification = "+clasification+" WHERE server_name LIKE '"+nameServer+"' AND name LIKE '"+nameManga+"'";
        //System.out.println(query);
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        //System.out.println("Guardado clasificación de "+nameManga+", siendo = " + String.valueOf(clasification));
        return true;
    }*/

    public boolean putListChaptersReaded(String nameServer, String nameManga, Vector<String> listChapters, int readed){
        SQLiteDatabase db = getWritableDatabase();

        /*db.execSQL("PRAGMA page_size = 4096");
        db.execSQL("PRAGMA cache_size = 16384");
        db.execSQL("PRAGMA temp_store = MEMORY");
        db.execSQL("PRAGMA journal_mode = OFF");
        db.execSQL("PRAGMA locking_mode = EXCLUSIVE");
        db.execSQL("PRAGMA synchronous = OFF");*/

        /*db.execSQL("PRAGMA page_size = 4096");
        db.execSQL("PRAGMA cache_size = 16384");
        db.execSQL("PRAGMA foreign_keys=OFF");
        db.execSQL("PRAGMA synchronous=OFF");*/

        //db.execSQL("PRAGMA journal_mode=MEMORY");
        //db.execSQL("PRAGMA default_cache_size=10000");
        //db.execSQL("PRAGMA locking_mode=EXCLUSIVE");

        db.beginTransaction();

        String query = "UPDATE chapter SET readed = "+readed+" WHERE server_name LIKE '" + nameServer + "' AND manga_name LIKE '" + nameManga + "'";
        String chapters = "";
        boolean primero = true;
        int i=0;
        for(String nameChapter : listChapters) {
            if (primero) {
                primero = false;
                chapters = "( name LIKE '" + nameChapter + "' ";
            } else {
                chapters += "OR name LIKE '" + nameChapter + "' ";
            }

            i++;

            if(i==100){
                primero = true;
                i=0;
                chapters += ")";
                try {
                    db.execSQL(query + " AND " + chapters);
                } catch (Exception e){
                    e.printStackTrace();
                    db.setTransactionSuccessful();
                    db.endTransaction();
                    return false;
                }
            }
        }
        if(!primero){
            chapters += ")";
            query += " AND " + chapters;
            try {
                db.execSQL(query);
            } catch (Exception e) {
                e.printStackTrace();
                db.setTransactionSuccessful();
                db.endTransaction();
                return false;
            }
        }

        /*for(String nameChapter : listChapters) {
            String query = "UPDATE chapter SET readed = "+readed+" WHERE server_name LIKE '" + nameServer + "' AND manga_name LIKE '" + nameManga + "' AND name LIKE '" + nameChapter + "'";
            try {
                db.execSQL(query);
            } catch (Exception e) {
                e.printStackTrace();
                db.setTransactionSuccessful();
                db.endTransaction();
                return false;
            }
        }*/
        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    public boolean putOnChapterReaded(String nameServer, String nameManga, String nameChapter){
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE chapter SET readed = 1 WHERE server_name LIKE '"+nameServer+"' AND manga_name LIKE '"+nameManga+"' AND name LIKE '"+nameChapter+"'";
        //System.out.println(query);
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean removeChapterReaded(String nameServer, String nameManga, String nameChapter){
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE chapter SET readed = 0 WHERE server_name LIKE '"+nameServer+"' AND manga_name LIKE '"+nameManga+"' AND name LIKE '"+nameChapter+"'";
        //System.out.println(query);
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Vector<Boolean> getListChaptersReaded_old(String nameServer, String nameManga){
        Vector<Boolean> chaptersReaded = new Vector<Boolean>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT readed FROM chapter WHERE server_name LIKE '"+nameServer+"' AND manga_name LIKE '"+nameManga+"'";
        Cursor cursor = db.rawQuery(query, null);
        while(cursor.moveToNext()){
            chaptersReaded.add(cursor.getInt(0) != 0);
        }
        cursor.close();
        if(chaptersReaded.isEmpty()){
            System.out.println("La lista de leídos está vacía");
            return null;
        }
        Collections.reverse(chaptersReaded);
        return chaptersReaded;
    }

    public Boolean chapterReaded(String nameServer, String nameManga, String nameChapter){
        Boolean readed = false;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT readed FROM chapter WHERE server_name LIKE '"+nameServer+"' AND manga_name LIKE '"+nameManga+"' AND name LIKE '"+nameChapter+"'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToNext()){
            readed = new Boolean(cursor.getInt(0) != 0);
        }
        cursor.close();
        return readed;
    }

    public Hashtable<String, Boolean> getListChaptersReaded(String nameServer, String nameManga){
        Hashtable<String, Boolean> chaptersReaded = new Hashtable<String, Boolean>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT name, readed FROM chapter WHERE server_name LIKE '"+nameServer+"' AND manga_name LIKE '"+nameManga+"'";
        Cursor cursor = db.rawQuery(query, null);
        while(cursor.moveToNext()){
            chaptersReaded.put(cursor.getString(0).toString(), new Boolean(cursor.getInt(1) != 0));
        }
        cursor.close();
        //Log.d("DEBUG_NOF","server: " + nameServer + ", manga: " + nameManga + ", size: " + chaptersReaded.size());
        return chaptersReaded;
    }

    public Vector<String> getListNamesChaptersReaded(String nameServer, String nameManga){
        Vector<String> chaptersReaded = new Vector<String>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT name FROM chapter WHERE server_name LIKE '"+nameServer+"' AND manga_name LIKE '"+nameManga+"' AND readed = 1";
        Cursor cursor = db.rawQuery(query, null);
        while(cursor.moveToNext()){
            chaptersReaded.add(cursor.getString(0));
        }
        cursor.close();
        return chaptersReaded;
    }

    public Vector<MangaItem> getFavouriteMangas(){
        Vector<MangaItem> favourites = new Vector<MangaItem>();
        MangaItem manga;
        SQLiteDatabase db = getReadableDatabase();
        //String query = "SELECT object, server_name, name FROM manga WHERE favourite = 1 ORDER BY name";
        String query = "SELECT object FROM manga WHERE favourite = 1 ORDER BY name";
        Cursor cursor = db.rawQuery(query, null);
        while(cursor.moveToNext()){
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            String jsonString = cursor.getString(0);
            //String nameServer = cursor.getString(1);
            manga = null;
            if (jsonString != null) {
                manga = gson.fromJson(jsonString, MangaItem.class);
            }
            if(manga != null) {
                //manga.setNameServer(nameServer);
                favourites.add(manga);
            }
        }
        cursor.close();
        return favourites;
    }

    public boolean putOnMangaFavourite(String nameServer, String nameManga){
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE manga SET favourite = 1 WHERE server_name LIKE '"+nameServer+"' AND name LIKE '"+nameManga+"'";
        //System.out.println(query);
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean removeMangaFavourite(String nameServer, String nameManga){
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE manga SET favourite = 0 WHERE server_name LIKE '"+nameServer+"' AND name LIKE '"+nameManga+"'";
        //System.out.println(query);
        try {
            db.execSQL(query);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean isMangaFavourite(String nameServer, String nameManga){
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT favourite FROM manga WHERE server_name LIKE '"+nameServer+"' AND name LIKE '"+nameManga+"'";
        Integer res = null;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToNext()){
            res = cursor.getInt(0);
        }
        cursor.close();
        if(res == null){
            System.out.println("El manga no existe");
            return false;
        }
        else{
            if(res == 0) return false;
            else return true;
        }
    }

    public boolean existManga(String nameServer, String nameManga){
        boolean existe;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT object FROM manga WHERE server_name LIKE '" + nameServer + "' and name LIKE '" + nameManga + "'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToNext()){
            existe = true;
        }
        else{
            existe = false;
        }
        cursor.close();
        return existe;
    }

    public boolean updateFavourites(FavouriteItem listFavouriteWeb){
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        String query;
        for(int i=0;i<listFavouriteWeb.getList_mangas().size(); i++){
            query = "INSERT OR REPLACE INTO manga(name, server_name, favourite) VALUES('"+
                    listFavouriteWeb.getList_mangas().get(i).getManga_name()+"', '"+
                    listFavouriteWeb.getList_mangas().get(i).getServer_name()+"', 1)";
            try {
                db.execSQL(query);
            } catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }
}