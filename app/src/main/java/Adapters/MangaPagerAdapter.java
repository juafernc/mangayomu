package Adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.example.juanma.mangayomu.MangaActivity;

import java.util.Locale;
import java.util.Vector;

import Fragments.FragmentMangaInfo;
import Fragments.FragmentMangaListChapters;

/**
 * Created by juanma on 22/07/14.
 */

public class MangaPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_TABS = 2;
    Vector<Fragment> fragments;

    public MangaPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new Vector<Fragment>();
    }

    public Vector<Fragment> getFragments() {
        return fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
    }

    @Override
    public int getCount() {
        return NUM_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return "Información".toUpperCase(l);
            case 1:
                return "Capítulos".toUpperCase(l);
        }
        return null;
    }


}