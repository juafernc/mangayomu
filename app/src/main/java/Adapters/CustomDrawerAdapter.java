package Adapters;

/**
 * Created by juanma on 19/07/14.
 */
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.juanma.mangayomu.R;

import Items.DrawerItem;
import Items.ServersItem;
import Items.UserItem;

public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {

    private final int INDICE_SESION = 0;
    private final int INDICE_PERSONAL = 1;
    private final int INDICE_SERVIDORES = 5;

    Context context;
    Vector<DrawerItem> drawerItemList;
    int layoutResID;

    int stateProgressBar;

    SharedPreferences prefs_user;

    public CustomDrawerAdapter(Context context, int layoutResourceID, Vector<DrawerItem> drawerItemList) {
        super(context, layoutResourceID, drawerItemList);
        this.context = context;
        this.drawerItemList = drawerItemList;
        this.layoutResID = layoutResourceID;

        this.stateProgressBar = ProgressBar.VISIBLE;

        prefs_user = getContext().getSharedPreferences(UserItem.PREFERENCIAS_DE_USUARIO, Context.MODE_PRIVATE);
    }

    public void setDrawerItemList(Vector<DrawerItem> drawerItemList) {
        this.drawerItemList = drawerItemList;
        Log.d("DEBUG", "Tamaño Lista: " + this.drawerItemList.size());
        notifyDataSetChanged();
    }

    public void setStateProgressBar(int stateProgressBar) {
        this.stateProgressBar = stateProgressBar;
        notifyDataSetChanged();
    }

    public void updateChanges(){
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerItemHolder holder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            holder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);

            holder.headerLayout = (LinearLayout) view.findViewById(R.id.headerLayout);
            holder.itemLayout = (LinearLayout) view.findViewById(R.id.itemLayout);

            holder.tittle = (TextView) view.findViewById(R.id.drawerTitle);
            holder.serverName = (TextView) view.findViewById(R.id.drawer_itemName);
            holder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            holder.progressBar = (ProgressBar) view.findViewById(R.id.progressBarMenu);

            view.setTag(holder);
        } else {
            holder = (DrawerItemHolder) view.getTag();
        }

        DrawerItem item = drawerItemList.get(position);

        if(item.isTittle()){
            holder.headerLayout.setVisibility(LinearLayout.VISIBLE);
            holder.itemLayout.setVisibility(LinearLayout.INVISIBLE);
            if(position == INDICE_SERVIDORES){
                System.out.println("###################### CustomListCenterAdapter, el estado de stateProgressBar es " + stateProgressBar);
                holder.progressBar.setVisibility(stateProgressBar);
            }

            holder.tittle.setText(item.getTittle());
        }
        else{
            holder.headerLayout.setVisibility(LinearLayout.INVISIBLE);
            holder.itemLayout.setVisibility(LinearLayout.VISIBLE);

            holder.serverName.setText(item.getServer());

            if(position == INDICE_SESION){
                String name_user = prefs_user.getString("USER_NAME", null);
                if(name_user != null){
                    holder.serverName.setText(name_user+"\n(Logout)");
                    holder.icon.setImageResource(android.R.drawable.presence_online);
                }
                else{
                    holder.serverName.setText("Login");
                    holder.icon.setImageResource(android.R.drawable.presence_offline);
                }
            }
            else if(position == INDICE_PERSONAL + 1){
                holder.icon.setImageResource(android.R.drawable.star_big_off);
            }
            else if(position == INDICE_PERSONAL + 2){
                holder.icon.setImageResource(android.R.drawable.stat_sys_download_done);
            }
            else if(position == INDICE_PERSONAL + 3){
                holder.icon.setImageResource(R.drawable.ic_action_group);
            }
            else if(position >= INDICE_SERVIDORES + 1){
                holder.icon.setImageResource(android.R.drawable.ic_menu_compass);
            }
        }


        return view;
    }

    private static class DrawerItemHolder {
        TextView serverName;
        TextView tittle;
        ImageView icon;
        ProgressBar progressBar;

        LinearLayout headerLayout, itemLayout;
    }
}