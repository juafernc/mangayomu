package Adapters;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.juanma.mangayomu.CapitulosActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Vector;

import Fragments.FragmentChapter;
import Items.UserItem;
import dataBase.DataBase;
import red.Utils;
import util.Hash;

/**
 * Created by juanma on 24/07/14.
 */
public class ChapterPagerAdapter extends FragmentPagerAdapter {

    //private static int NUM_TABS = 5;

    private Vector<FragmentChapter> fragments;
    private Activity activity;

    private DataBase db;

    public ChapterPagerAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        fragments = new Vector<FragmentChapter>();
        this.activity = activity;
        db = new DataBase(activity);
        //fragments.setSize(CapitulosActivity.chapter.getList_images().size());
    }

    public Vector<FragmentChapter> getFragments() {
        return fragments;
    }

    public void addFragment(FragmentChapter fragment) {
        fragments.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        //return FragmentChapter.newInstance(position);
        /*if(fragments.get(position) == null) {
            fragments.set(position, FragmentChapter.newInstance(position));
            fragments.get(position).setPosition(position);
            return fragments.get(position);
        }
        else{
            fragments.get(position).setPosition(position);
            return fragments.get(position);
        }*/
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return CapitulosActivity.chapter.getList_images().size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        // Eliminamos la referencia a la imagen, para que no consuma memoria mientras no se vea.
        fragments.get(position).image = null;
        Log.d("DEBUG_PAGER", CapitulosActivity.mViewPager.getCurrentItem() +"/"+ (CapitulosActivity.chapter.getList_images().size()-1));
        updateChapterReaded();
    }

    void updateChapterReaded(){
        if(CapitulosActivity.mViewPager.getCurrentItem() == CapitulosActivity.chapter.getList_images().size()-1){
            if(!db.chapterReaded(CapitulosActivity.nameServer, CapitulosActivity.nameManga, CapitulosActivity.nameChapter)){
                if(db.putOnChapterReaded(CapitulosActivity.nameServer, CapitulosActivity.nameManga, CapitulosActivity.nameChapter)){
                    // actualizamos el servidor
                    new UpdateChapterReadedOnServer(CapitulosActivity.nameServer, CapitulosActivity.nameManga, CapitulosActivity.nameChapter).execute();
                    Toast.makeText(activity, "He llegado a la última imagen del manga " + CapitulosActivity.nameChapter, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    class UpdateChapterReadedOnServer extends AsyncTask<Void, Void, String> {

        String server_name;
        String manga_name;
        String chapter_name;

        SharedPreferences prefs_user;
        SharedPreferences.Editor editor_user;

        UpdateChapterReadedOnServer(String server_name, String manga_name, String chapter_name){
            this.server_name = server_name;
            this.manga_name = manga_name;
            this.chapter_name = chapter_name;

            prefs_user = activity.getSharedPreferences(UserItem.PREFERENCIAS_DE_USUARIO, Context.MODE_PRIVATE);
            editor_user = prefs_user.edit();
        }

        @Override
        protected String doInBackground(Void... voids) {

            if(db.isMangaFavourite(server_name, manga_name)) {
                if (!Utils.verificaConexion(activity)) {
                    cancel(true);
                    return null;
                } else {
                    String user_name = prefs_user.getString("USER_NAME", null);
                    // Si estamos logueados
                    if (user_name != null) {
                        String client_id = prefs_user.getString("CLIENT_ID", null);
                        String pin = prefs_user.getString("PIN", null);
                        String next_random = prefs_user.getString("NEXT_RANDOM", null);
                        String key = Hash.md5(pin + next_random);

                        String responseString = Utils.modifyChapterReaded(
                                user_name,
                                client_id,
                                key,
                                server_name,
                                manga_name,
                                chapter_name,
                                "add"
                        );

                        if (responseString != null) {
                            UserItem userItem = new UserItem();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();
                            try {
                                userItem = gson.fromJson(responseString, UserItem.class);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (userItem != null) {
                                editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                                editor_user.commit();
                            }

                        }
                    }
                }
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(activity, "No hay conexión a internet, los cambios realizados no se sincronizarán", Toast.LENGTH_SHORT).show();
        }

    }

}
