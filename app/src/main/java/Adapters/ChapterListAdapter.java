package Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.juanma.mangayomu.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Hashtable;

import Items.MangaItem;
import Items.UserItem;
import dataBase.DataBase;
import red.Utils;
import util.Hash;

/**
 * Created by juanma on 22/07/14.
 */
public class ChapterListAdapter extends BaseAdapter {

    public static class HolderView{
        Button downloadIcon;
        TextView nameChapter;
        Button readedButton;
    }

    MangaItem mangaItem;
    Activity activity;

    String nameServer;

    DataBase db;
    Hashtable<String, Boolean> chapterReaded;

    SharedPreferences prefs_user;
    SharedPreferences.Editor editor_user;

    public ChapterListAdapter(Activity activity, MangaItem mangaItem, String nameServer){
        this.activity = activity;
        this.mangaItem = mangaItem;
        this.nameServer = nameServer;

        prefs_user = activity.getSharedPreferences(UserItem.PREFERENCIAS_DE_USUARIO, Context.MODE_PRIVATE);
        editor_user = prefs_user.edit();

        db = new DataBase(activity);
        chapterReaded = db.getListChaptersReaded(nameServer, mangaItem.getName());
        Log.d("DEBUG_NOF","ChapterListAdapter, size: " + ((chapterReaded == null)? null : chapterReaded.size()));
    }

    public void setMangaItem(MangaItem mangaItem) {
        this.mangaItem = mangaItem;
        /*Log.d("DEBUG", "Actualizado mangaItem");
        for(String mm : mangaItem.getList_chapters()){
            Log.v("DEBUG", mm);
        }*/
        Log.d("DEBUG_ACT","ACTUALIZADO");
        notifyDataSetChanged();
    }

    public void UpdateList(){
        chapterReaded = db.getListChaptersReaded(nameServer, mangaItem.getName());
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mangaItem.getList_chapters().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final HolderView holder;

        if(view == null){
            LayoutInflater inflater = (activity).getLayoutInflater();
            holder = new HolderView();

            view = inflater.inflate(R.layout.list_chapters_item, viewGroup, false);
            holder.downloadIcon = (Button) view.findViewById(R.id.download_chapter_icon);
            holder.nameChapter = (TextView) view.findViewById(R.id.nameChapter);
            holder.readedButton = (Button) view.findViewById(R.id.chapter_viewed_icon);

            view.setTag(holder);
        }
        else{
            holder = (HolderView) view.getTag();
        }

        holder.nameChapter.setText(mangaItem.getList_chapters().get(position));
        //Log.d("DEBUG_NOF","chapterReaded: " + chapterReaded);
        if(chapterReaded != null && chapterReaded.get(mangaItem.getList_chapters().get(position)) != null) {
            Log.d("DEBUG_NOF","chapterReaded.get("+mangaItem.getList_chapters().get(position)+"): " + chapterReaded.get(mangaItem.getList_chapters().get(position)));
            if (chapterReaded.get(mangaItem.getList_chapters().get(position))) {
                holder.readedButton.setBackgroundResource(R.drawable.readed);
            } else {
                holder.readedButton.setBackgroundResource(R.drawable.unreaded);
            }
        }

        holder.readedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(chapterReaded != null) {
                    if (chapterReaded.get(mangaItem.getList_chapters().get(position))) {
                        if(db.removeChapterReaded(nameServer, mangaItem.getName(), mangaItem.getList_chapters().get(position))){
                            chapterReaded.put(mangaItem.getList_chapters().get(position), false);
                            new UpdateChapterReadedOnServer(nameServer, mangaItem.getName(), mangaItem.getList_chapters().get(position), false, holder.readedButton).execute();
                            System.out.println("Marcar capítulo como no leído");
                        }
                    } else {
                        if(db.putOnChapterReaded(nameServer, mangaItem.getName(), mangaItem.getList_chapters().get(position))){
                            chapterReaded.put(mangaItem.getList_chapters().get(position), true);
                            new UpdateChapterReadedOnServer(nameServer, mangaItem.getName(), mangaItem.getList_chapters().get(position), true, holder.readedButton).execute();
                            System.out.println("Marcar capítulo como leído");
                        }
                    }
                }
            }
        });

        return view;
    }

    class UpdateChapterReadedOnServer extends AsyncTask<Void, Void, String> {

        String server_name;
        String manga_name;
        String chapter_name;
        boolean insert;
        Button buttonReaded;
        //ProgressDialog dialog;
        String type;

        UpdateChapterReadedOnServer(String server_name, String manga_name, String chapter_name, boolean insert, Button buttonReaded){
            this.server_name = server_name;
            this.manga_name = manga_name;
            this.chapter_name = chapter_name;
            this.insert = insert;
            this.buttonReaded = buttonReaded;
            if(insert){
                type = "add";
            }
            else{
                type = "delete";
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Diálogo de espera para esperar a que el servidor responda
            /*dialog = new ProgressDialog(activity);
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.show();*/
            buttonReaded.setBackgroundColor(Color.TRANSPARENT);
            buttonReaded.setEnabled(false);
        }

        @Override
        protected String doInBackground(Void... voids) {

            if(db.isMangaFavourite(server_name, manga_name)) {
                if (!Utils.verificaConexion(activity)) {
                    cancel(true);
                    return null;
                } else {
                    String user_name = prefs_user.getString("USER_NAME", null);
                    // Si estamos logueados
                    if (user_name != null) {
                        String client_id = prefs_user.getString("CLIENT_ID", null);
                        String pin = prefs_user.getString("PIN", null);
                        String next_random = prefs_user.getString("NEXT_RANDOM", null);
                        String key = Hash.md5(pin + next_random);

                        String responseString = Utils.modifyChapterReaded(
                                user_name,
                                client_id,
                                key,
                                server_name,
                                manga_name,
                                chapter_name,
                                type
                        );

                        if (responseString != null) {
                            UserItem userItem = new UserItem();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();
                            try {
                                userItem = gson.fromJson(responseString, UserItem.class);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (userItem != null) {
                                editor_user.putString("NEXT_RANDOM", userItem.getNext_random());
                                editor_user.commit();
                            }

                        }
                    }
                }
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(activity, "No hay conexión a internet, los cambios realizados no se sincronizarán", Toast.LENGTH_SHORT).show();
            if(insert){
                buttonReaded.setBackgroundResource(R.drawable.readed);
            }
            else{
                buttonReaded.setBackgroundResource(R.drawable.unreaded);
            }
            buttonReaded.setEnabled(true);
            //dialog.dismiss();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //dialog.dismiss();
            if(insert){
                buttonReaded.setBackgroundResource(R.drawable.readed);
            }
            else{
                buttonReaded.setBackgroundResource(R.drawable.unreaded);
            }
            buttonReaded.setEnabled(true);
        }

    }
}
