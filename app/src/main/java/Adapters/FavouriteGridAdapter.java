package Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juanma.mangayomu.R;

import java.util.Vector;

import Items.MangaItem;
import dataBase.DataBase;
import red.ImageDownloader;
import red.ImageDownloaderMem;

/**
 * Created by juanma on 19/08/14.
 */
public class FavouriteGridAdapter extends BaseAdapter{

    static class HolderView{
        TextView manga;
        TextView server;
        ImageView image;
    }

    Activity activity;
    Vector<MangaItem> favouriteMangas;

    private ImageDownloaderMem imageDownloaderMem;
    //private ImageDownloader imageDownloader;

    DataBase db;

    public FavouriteGridAdapter(Activity activity) {
        this.activity = activity;
        db = new DataBase(activity);
        favouriteMangas = db.getFavouriteMangas();
        imageDownloaderMem = new ImageDownloaderMem(activity.getFilesDir());
        //imageDownloader = new ImageDownloader();
    }

    public boolean removeElement(int i){
        try {
            favouriteMangas.remove(i);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public int getCount() {
        return favouriteMangas.size();
    }

    @Override
    public Object getItem(int i) {
        return favouriteMangas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        HolderView holder;

        if(view == null){
            LayoutInflater inflater = activity.getLayoutInflater();
            holder = new HolderView();

            view = inflater.inflate(R.layout.custom_favourite_item, viewGroup, false);
            holder.image = (ImageView) view.findViewById(R.id.imageManga_favourite);
            holder.manga = (TextView) view.findViewById(R.id.tituloManga_favourite);
            holder.server = (TextView) view.findViewById(R.id.tituloServidor_favourite);

            view.setTag(holder);
        }
        else{
            holder = (HolderView) view.getTag();
        }

        if(favouriteMangas.get(position).getUrl_image() != null){
            //imageDownloader.download(favouriteMangas.get(position).getUrl_image(), holder.image);
            imageDownloaderMem.download_to_mem(favouriteMangas.get(position).getUrl_image(),
                    favouriteMangas.get(position).getServer_name(),
                    favouriteMangas.get(position).getName(),
                    holder.image);
        }
        else{
            holder.image.setImageResource(R.drawable.imagen_no_disponible);
        }
        if(favouriteMangas.get(position).getName() != null){
            holder.manga.setText(favouriteMangas.get(position).getName());
        }
        else{
            holder.manga.setText("Nombre no disponible");
        }
        if(favouriteMangas.get(position).getServer_name() != null){
            holder.server.setText(favouriteMangas.get(position).getServer_name());
        }
        else{
            holder.server.setText("Servidor no disponible");
        }




        return view;
    }
}
