package Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.example.juanma.mangayomu.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by juanma on 21/07/14.
 */
public class CustomListCenterAdapter extends ArrayAdapter<String> {

    Activity actividad;
    List<String> list;
    List<String> list_filter;
    Filter filter;

    public CustomListCenterAdapter(Activity activity, Vector<String> list){
        super(activity, R.layout.custom_element_list_item, list);
        //super(activity, list);
        this.actividad = activity;
        this.list = list;
        this.list_filter = list;
    }

    @Override
    public int getCount() {
        return list_filter.size();
    }

    @Override
    public String getItem(int i) {
        return list_filter.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        HolderView holder;

        if(view == null){
            LayoutInflater inflater = (actividad).getLayoutInflater();
            holder = new HolderView();

            view = inflater.inflate(R.layout.custom_element_list_item, viewGroup, false);
            holder.textView = (TextView) view.findViewById(R.id.textViewList);

            view.setTag(holder);
        }
        else{
            holder = (HolderView) view.getTag();
        }

        holder.textView.setText(list_filter.get(position));

        return view;
    }

    private static class HolderView{
        TextView textView;
    }

    @Override
    public Filter getFilter() {
        if(filter == null){
            filter = new filter_here();
        }
        return filter;
    }

    public class filter_here extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            FilterResults Result = new FilterResults();
            // if constraint is empty return the original names
            if(constraint.length() == 0 ){
                Result.values = list;
                Result.count = list.size();
                return Result;
            }

            ArrayList<String> Filtered_Names = new ArrayList<String>();
            String filterString = constraint.toString().toLowerCase();
            String filterableString;

            for(int i = 0; i<list.size(); i++){
                filterableString = list.get(i);
                if(filterableString.toLowerCase().contains(filterString)){
                    Filtered_Names.add(filterableString);
                }
            }
            Result.values = Filtered_Names;
            Result.count = Filtered_Names.size();

            return Result;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            // TODO Auto-generated method stub
            list_filter = (List<String>) results.values;
            notifyDataSetChanged();
        }

    }
}
